package com.ford.automation.CommonAPI;
import java.net.URL;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.interactions.HasTouchScreen;
import org.openqa.selenium.interactions.TouchScreen;
import org.openqa.selenium.remote.RemoteTouchScreen;

import io.appium.java_client.android.AndroidDriver;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class SwipeableWebDriver extends AndroidDriver implements HasTouchScreen {
	private RemoteTouchScreen touch;

	    public SwipeableWebDriver(URL remoteAddress, Capabilities desiredCapabilities) {
	        super(remoteAddress, desiredCapabilities);
	        touch = new RemoteTouchScreen(getExecuteMethod());
	    }

	    public TouchScreen getTouch() {
	        return touch;
	    }
}