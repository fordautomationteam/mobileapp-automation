package com.ford.automation.CommonAPI;

public class ElementObjectMapData extends SeleniumCommonAPI {
	String Page = "";
	String ObjName = "";
	String MinVer = "";
	String MaxVer = "";
	String TestVer = "";
	
	String ViewMode ="";
	String SrcType = "";
	String MultiEnable ="";
	String SourceValue = "";
	int ElementId = -1;
	public  final  int PAGE_LOC =0;
	public  final  int OBJ_NAME_LOC = 1;
	public final   int MIN_VER_LOC = 2;
	public final   int MAX_VER_LOC = 3;
	public final   int VIEW_MODE_LOC = 4;
	public final   int SRC_TYPE_LOC = 5;
	public final   int MULTI_ENABLE =6;
	public final int ELMEENT_ID_LOC =7;
	public final int SOURCE_VAL_LOC = 8;
	
	public ElementObjectMapData(String aPage, String aObjName, String aTestVer)
	{
		Page = aPage;
		ObjName = aObjName;
		TestVer = aTestVer;
	}
	public String getPage()
	{
		if (Page.equals("") == true)
		{
			return null;
		}
	
		return Page;
	}
	public String getObjName()
	{
		if(ObjName.equals("")== true)
		{
			return null;
		}
		return ObjName;
	}
	public String getMinVer()
	{
		if(MinVer.equals("")== true)
		{
			return null;
		}
		return MinVer;
	}
	public String getMaxVer()
	{
		if(MaxVer.equals("")== true)
		{
			return null;
		}
		return MaxVer;
	}
	public String getTestVer()
	{
		if(TestVer.equals("")== true)
		{
			return null;
		}
		return TestVer;
	}
	public String getViewMode()
	{
		if(ViewMode.equals("")== true)
		{
			return null;
		}
		return ViewMode;
	}
	public String getSrcType()
	{
		if (SrcType.equals("") == true)
		{
			return null;
		}
		return SrcType;
	}
	public String getMultiEnable()
	{
		if(MultiEnable.equals("")== true)
		{
			return null;
		}
		return MultiEnable;
	}
	public int getElementId()
	{
		return ElementId;
	}
	public String getSourceValue()
	{
		if(SourceValue.equals("") == true)
		{
			return null;
		}
		return SourceValue;
	}
	
	
	public int populateElementData(String objMapData[][])
	{
		int verTestMin =0;
		int verTestMax =0;
		int rc = -1;
		for(int i=1;i<objMapData.length;i++)
		{
			if(Page.equals(objMapData[i][PAGE_LOC]) == true)
			{
				if(ObjName.equals(objMapData[i][OBJ_NAME_LOC]) == true)
				{
					verTestMin=appVersionCompare(TestVer,objMapData[i][MIN_VER_LOC]);
					verTestMax=appVersionCompare(TestVer,objMapData[i][MAX_VER_LOC]);
					//verTestMin = TestVer.compareTo(objMapData[i][MIN_VER_LOC]);
					//verTestMax = TestVer.compareTo(objMapData[i][MAX_VER_LOC]);
					if ((verTestMin >=0) && (verTestMax <= 0))
					{
						//we found the item
						MinVer = objMapData[i][MIN_VER_LOC];
						MaxVer = objMapData[i][MAX_VER_LOC];
						
						ViewMode =objMapData[i][VIEW_MODE_LOC];
						MultiEnable = objMapData[i][MULTI_ENABLE];
						SrcType =  objMapData[i][SRC_TYPE_LOC];
						ElementId = Integer.parseInt(objMapData[i][ELMEENT_ID_LOC]);
						SourceValue = objMapData[i][SOURCE_VAL_LOC];
						rc =0;
						break;
					}
					else
					{
						System.out.println("The Version is not correct\n");
						String LogMsg="\t\tThe Version is not correct";
						LogInBuffer.add(LogMsg);
						updateLogText(LogMsg+"\n");
					}
				}
			}
		}
		return rc;
	}
				
			
			

}
