package com.ford.automation.CommonAPI;
import java.awt.AWTException;
import java.awt.Robot;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.os.WindowsUtils;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.os.WindowsRegistryException;
import com.google.common.base.Function;
import io.appium.java_client.AppiumDriver;



//Contains common functions to find elements and for time delay
public class SeleniumCommonAPI extends GlobalParameters
{	
	//Parameter file constants that have the browser executable directories.
	public static String CHROME_EXEC_DIRECTORY="default";
	public static String FIREFOX_EXEC_DIRECTORY;
	
	//directory paths for the IE and Chrome driver files
	private static final String IE_DRIVER_PATH = "IEDriverServer.exe";
	private static final String CHROME_DRIVER_PATH = "chromedriver.exe";
	
	//IE driver service
	static InternetExplorerDriverService ieService;
	
	//Make webDriver class variable to allow screen shots to be taken of failed tests.
	public static WebDriver webDriver;
	
	//web test enbale flag
	public static boolean WEB_ENABLE = false;
	
	//10/13/15 web driver variable to read email in mail.com
	public static WebDriver emailDriver;
	
	// The test driver
	public static WebDriver TestDriver;
	
	//New constant name that replaces DEFAULT_WAIT_TIME above.  Specified in parameter file.
	public static int WAIT_FOR_ELEM_WAIT_TIME;
	
	//Maximum retries to wait for the globe image to appear on the login page.
	private static final int MAX_RETRIES = 6;
	
	//Make the browser type global to log the network errors
	public static String browserType;
	
	
	//Returns the current time when the method is called
	public static String getCurrentTimeWithDate()
	{
		DateFormat dateFormat = new SimpleDateFormat("hh:mm a MM/dd/yy");
		Date dt = new Date();
		return dateFormat.format(dt);
	}
	
	//Returns the current time when the method is called
	public static String getCurrentTimeWithSecond()
	{
		DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss MM/dd/yy");
		Date dt = new Date();
		return dateFormat.format(dt);
	}
	
	// Returns the current time when the method is called
	public static String getCurrentTime(String TimeFormat)
	{
		DateFormat dateFormat = new SimpleDateFormat(TimeFormat);
		Date dt = new Date();
		return dateFormat.format(dt);
	}
	
	
	//find web element by X path
	public static WebElement findElemByXPath(WebDriver webDriver, String xpath)
	{
		//Use findElement and catch the not found exception.
		try
		{
			WebElement element = webDriver.findElement(By.xpath(xpath));
			return element;
		}
		catch (WebDriverException e)
		{
			return null;
		}
	}
	
	
	//find web element by X path
	public static List<WebElement> findElementsByXPath(WebDriver webDriver, String xpath)
	{
		//Use findElement and catch the not found exception.
		try
		{
			List<WebElement> element = webDriver.findElements(By.xpath(xpath));
			return element;
		}
		catch (WebDriverException e)
		{
			return null;
		}
	}
	
	//find web element by Id
	public static WebElement findElemById(WebDriver webDriver, String elemId)
	{
		//Use findElement and catch the not found exception.
		try
		{
			WebElement element = webDriver.findElement(By.id(elemId));
			return element;
		}
		catch (WebDriverException e)
		{
			return null;
		}	
	}
	
	//find web element by Id
	public static List<WebElement> findElemsById(WebDriver webDriver, String elemId)
	{
		//Use findElement and catch the not found exception.
		try
		{
			List<WebElement> elements = webDriver.findElements(By.id(elemId));
			return elements;
		}
		catch (WebDriverException e)
		{
			return null;
		}	
	}
	
	
	//find web element by Class
	public static WebElement findElemByClass(WebDriver webDriver, String className)
	{
		//Use findElement and catch the not found exception.
		try
		{
			WebElement element = webDriver.findElement(By.className(className));
			return element;
		}
		catch (WebDriverException e)
		{
			return null;
		}	
	}
	
	/*//find web element by Class
	public static WebElement findElemByLinkText(WebDriver webDriver, String className)
	{
		//Use findElement and catch the not found exception.
		try
		{
			WebElement element = webDriver.findElement(By.);
			return element;
		}
		catch (WebDriverException e)
		{
			return null;
		}	
	}*/
	
	//10/9/15 Find multiple elements by class
	public static List<WebElement> findElementsByClass(WebDriver webDriver, String className)
	{
		//Use findElement and catch the not found exception.
		try
		{
			List<WebElement> elements = webDriver.findElements(By.className(className));
			
			return elements;
		}
		catch (WebDriverException e)
		{
			return null;
		}	
	}
	
	//10/9/15 Find multiple elements by class
	public static List<WebElement> findElementsByName(WebDriver webDriver, String Name)
	{
		//Use findElement and catch the not found exception.
		try
		{
			List<WebElement> elements = webDriver.findElements(By.name(Name));
			return elements;
		}
		catch (WebDriverException e)
		{
			return null;
		}	
	}
	
	
	//find web element by Name
	public static WebElement findElemByName(WebDriver webDriver, String name)
	{
		//Use findElement and catch the not found exception.
		try
		{
			WebElement element = webDriver.findElement(By.name(name));
			return element;
		}
		catch (WebDriverException e)
		{
			return null;
		}	
	}
		
	
	//find web element by link text (for html <a> stuff)
	public WebElement findElemLinkText(WebDriver webDriver, String linkText)
	{
		//Use findElement and catch the not found exception.
		try
		{
			WebElement element = webDriver.findElement(By.linkText(linkText));
			return element;
		}
		catch (WebDriverException e)
		{
			return null;
		}	
	}
	
	
	//find web element by link text (for html <a> stuff)
	public static WebElement findElemByTag(WebDriver webDriver, String tagName)
	{
		//Use findElement and catch the not found exception.
		try
		{
			WebElement element = webDriver.findElement(By.tagName(tagName));
			return element;
		}
		catch (WebDriverException e)
		{
			return null;
		}	
	}	
	
	//objectExistsWithXpath() check the object exists
	//return true, the object exists, or object doesn't exist
	public static boolean objectExistsWithXpath(WebDriver driver, String Xpath)
	{
		try 
		{
			WebElement Element = findElemByXPath(driver,Xpath);
			if(Element!=null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
	}
	
	
	//objectExistsWithXpath() check the object exists
	//return true, the object exists, or object doesn't exist
	public static boolean objectExistsWithId(WebDriver driver, String ID)
	{
		try 
		{
			WebElement Element = findElemById(driver,ID);
			if(Element!=null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
	}
	
	//objectExistsWithText() check the object exists
	//return true, the object exists, or object doesn't exist
	public static boolean objectExistsWithClassName(WebDriver driver, String ClassName)
	{
		try 
		{
			String Xpath="//*[@class='"+ClassName+"']";
			WebElement Element = findElemByXPath(driver,Xpath);
			if(Element!=null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
	}
	
	//Wait for an element to appear by waiting for the link text.  The waitTime is in seconds
	public static WebElement waitForElementName(WebDriver driver, int waitTime, String name)
	{
		//Declare element as a variable to catch the timeout exception.
		WebElement element;
		
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		
		//Catch the timeout exception if we don't find the element.
		try
		{
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
		}
		catch (TimeoutException e) 
		{
			element = null;
		}
		
		return element;	
	}
	
	//objectExistsWithText() check the object exists
	//return true, the object exists, or object doesn't exist
	public static boolean objectExistsWithText(WebDriver driver, String xpath,String findText)
	{
		try 
		{
			WebElement textElement = findElemByXPath(driver,xpath);
			if(textElement!=null)
			{
				if(textElement.getText().equals(findText))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
	}
	
	//Wait for a web element to be visible up to the waitTime.
	//waitTime is seconds.
	public static WebElement waitForElementXpath(WebDriver driver, int waitTime, String xpath)
	{
		//Declare element as a variable to catch the timeout exception.
		WebElement element;
		
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		
		//Catch the timeout exception if we don't find the element.
		try
		{
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		}
		catch (TimeoutException e) 
		{
			element = null;
		}

		return element;	
	}
	
	
	//Wait for an element to appear by waiting for the element id.  The waitTime is in seconds
	public static WebElement waitForElementId(WebDriver driver, int waitTime, String id)
	{
		//Declare element as a variable to catch the timeout exception.
		WebElement element;
		
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		
		//Catch the timeout exception if we don't find the element.
		try
		{
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
		}
		catch (TimeoutException e) 
		{
			element = null;
		}
		
		return element;	
	}
	
	
	//Wait for an element to appear by waiting for the link text.  The waitTime is in seconds
	public WebElement waitForElementLinkText(WebDriver driver, int waitTime, String linkText)
	{
		//Declare element as a variable to catch the timeout exception.
		WebElement element;
		
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		
		//Catch the timeout exception if we don't find the element.
		try
		{
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(linkText)));
		}
		catch (TimeoutException e) 
		{
			element = null;
		}
		
		return element;	
	}
	
	
	//Wait for a web element with a given text string to be visible up to the waitTime.
	//Return true if element with the text is present.
	//Return false if text doesn't match or not found in time.
    public static boolean waitForElementText(WebDriver driver,int waitTime, String xpath,String findText)
	{
    	//Set the maximum wait time
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		
		//Find the text element
		WebElement textElement = findElemByXPath(driver,xpath);
		//Wait for the element and the text.  Need to catch the timeout exception.
		try
		{
		  boolean msgFound = wait.until(ExpectedConditions.textToBePresentInElement(textElement,findText));
		  return msgFound;
		}
		catch (TimeoutException e)
		{
			return false;
		}
	}
	
	//Copy of the verifyWebConnect function with extra parameter to pass in Xpath.
	//This is used to access the shark lasers website to get temporary email for 
	//the registration text.
	public boolean getGeneralWebsite(String websiteURL,String browserName,int maxTimeout,String xPath,String waitText)
	{
		//If true, the globe image web element was found
		boolean found = false;
		
		//Retry counter.
		int retryCount = 0;
		
		//If true, continue looping
		boolean cont = true;
		
		//Get the webDriver
		getWebDriver(websiteURL,browserName,maxTimeout);
		
		while (cont)
		{
			//xpath is Ford EV owner? link text
			found = waitForElementText(webDriver,WAIT_FOR_ELEM_WAIT_TIME,xPath,waitText);
			
			if (!found)
			{
				retryCount++;
				
				if (retryCount > MAX_RETRIES)
					cont = false;
				else
				{
					//Stop the web driver
					webDriver.quit();
					
					//Wait a second
					delay(1000);
					
					//Create a new web driver
					getWebDriver(websiteURL,browserName,maxTimeout);
				}
			}
			else
			{
				cont = false;
			}	
		}
		return found;
	}
	
	
	//Verify a connection to the MyFord Mobile website login page by waiting for the element text 
	//"Ford EV owner?".  If the text is not found within one minute, try up to three times.
	public boolean verifyWebConnect(String websiteURL,String browserName,int maxTimeout)
	{
		//If true, the globe image web element was found
		boolean found = false;
		
		//Retry counter.
		int retryCount = 0;
		
		//If true, continue looping
		boolean cont = true;
		
		//Get the webDriver
		getWebDriver(websiteURL,browserName,maxTimeout);
		
		while (cont)
		{
			//Xpath is Ford EV owner? link text
			found = waitForElementText(webDriver,WAIT_FOR_ELEM_WAIT_TIME ,"html/body/div/div[4]/div/span/span","Ford EV owner?");
			
			if (!found)
			{
				retryCount++;
				
				if (retryCount > MAX_RETRIES)
					cont = false;
				else
				{
					//Stop the web driver
					webDriver.quit();
					
					//Wait a second
					delay(1000);
					
					//Create a new web driver
					getWebDriver(websiteURL,browserName,maxTimeout);
				}
			}
			else
			{
				cont = false;
			}	
		}
		return found;
	}

	
	//Wait function - wait in time mili-seconds
	public static void delay(int time)
	{
		if(TestStatusFlag)
		{
			try 
			{
				Thread.sleep(time);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	
	//Count the number of list elements.
	public int countListElements(WebDriver webDriver, String xpath)
	{
		List<WebElement> elements = webDriver.findElements(By.xpath(xpath));
		return elements.size();	
	}
	
	
	//Create a chrome web driver.
	private static WebDriver createChromeDriver()
	{	
		System.setProperty("webdriver.chrome.driver",CHROME_DRIVER_PATH);
		
		
		
	    //Get the capabilities: maximize browser window, eliminate the certificate error message
	    DesiredCapabilities capabilities = createChromeCapabilities();
	
	    //Create the chrome driver service
	    ChromeDriverService srvc = createChromeDriverService();
	    
	    //Create the web driver.
		return new ChromeDriver(srvc,capabilities);
	}
	
	
	//Create the capabilities for the chrome driver
	private static DesiredCapabilities createChromeCapabilities()
	{
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	    ChromeOptions options = new ChromeOptions();
	    options.addArguments("test-type");
	    
	    //Maximize the driver window
	    options.addArguments("start-maximized");
	    
	    //1/13/15 Set the chrome browser executable directory if it's not equal to "default"
	    if (!CHROME_EXEC_DIRECTORY.toLowerCase().equals("default"))
	    {
	    	//System.out.println("SET_CHROME_DIRECTOR: IS "+CHROME_EXEC_DIRECTORY);
	    	options.setBinary(CHROME_EXEC_DIRECTORY);
	    }
	    
	    capabilities.setCapability("chrome.binary",CHROME_DRIVER_PATH);
	    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
	    
	    return capabilities;
	}
	
	
	//Create chrome driver service
	private static ChromeDriverService createChromeDriverService()
	{
		//Create chrome driver service
	    ChromeDriverService.Builder builder = new ChromeDriverService.Builder();
	    
	    //USE PORT 9515
	    //ChromeDriverService srvc = builder.usingDriverExecutable(new File(CHROME_DRIVER_PATH)).usingPort(chromePortNumber).build();
	    
	    ChromeDriverService srvc = builder.usingDriverExecutable(new File("chromedriver.exe")).usingAnyFreePort().build();
	    
	    try 
	    {
	        srvc.start();
	        return srvc;
	    } 
	    catch (IOException e) 
	    {
	    	System.out.println("COMMON_FUNCTIONS: Failed to create chrome driver service");
	        return null;
	    }  
	}
	
	
	//Create a IE (internet explorer) web driver.
	private static WebDriver createIEDriver(String websiteURL)
	{	
		System.setProperty("webdriver.ie.driver",IE_DRIVER_PATH);
		
	    //Get the capabilities: maximize browser window, eliminate the certificate error message
	    DesiredCapabilities capabilities = createIECapabilities(websiteURL);
	    
	    //Create the IE driver service
	    ieService = createIEDriverService();
	    
	    WebDriver driver = new InternetExplorerDriver(ieService,capabilities);	
	    
	    //Maximize the browser
	    driver.manage().window().maximize();

		return driver;
	}	
	
	
	//Stop the ieService
	public void stopIEService()
	{
		ieService.stop();
	}
	
	
	//Create the capabilities for the internet explorer driver
	private static DesiredCapabilities createIECapabilities(String websiteURL)
	{
		//Create the IE capabilities object
		DesiredCapabilities ieCapab = DesiredCapabilities.internetExplorer();

		//Ignore the security domains
		ieCapab.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
		
		//Turn off native elements
		ieCapab.setCapability("nativeEvents", false);
		
		//Set the browser website
		ieCapab.setCapability("initialBrowserUrl",websiteURL); 
		
		return ieCapab;
	}
	
	
	//Create IE driver service
	private static InternetExplorerDriverService createIEDriverService()
	{
		InternetExplorerDriverService.Builder builder= new InternetExplorerDriverService.Builder();
		//InternetExplorerDriverService srvc = builder.usingPort(iePortNumber).withHost("127.0.0.1").build();	
		InternetExplorerDriverService srvc = builder.usingAnyFreePort().withHost("127.0.0.1").build();	
		
		return srvc;
	}
	
	//10/13/15 Get chrome webdriver to read email
	public void getEmailDriver(String websiteURL,int maxTimeout)
	{	
		//Create the web driver.
		emailDriver = createChromeDriver();
				
		//Set the maximum timeout to locate a web element
		emailDriver.manage().timeouts().implicitlyWait(maxTimeout, TimeUnit.SECONDS);
				
		//Get the website name
		emailDriver.get(websiteURL);			
	}
	
	//Creates a web driver object based on the type parameter passed in: chrome, firefox, IE, safari
	public static void getWebDriver(String websiteURL,String browser,int maxTimeout)
	{
		//Create the chrome driver
		if (browser.contains("chrome"))
		{
			
			//Create the web driver.
			webDriver = createChromeDriver();
			
			//Set the maximum timeout to locate a web element
			webDriver.manage().timeouts().implicitlyWait(maxTimeout, TimeUnit.SECONDS);
			
			//Get the website name
			webDriver.get(websiteURL);		
		}
		
		//Create fire fox driver.
		else if (browser.contains("firefox"))
		{
			//Add code if the firefox is installed in a directory other than the default one
			if (!FIREFOX_EXEC_DIRECTORY.toLowerCase().equals("default"))
			{
				FirefoxBinary binary = new FirefoxBinary(new File(FIREFOX_EXEC_DIRECTORY));
				FirefoxProfile profile = new FirefoxProfile();
				webDriver = new FirefoxDriver(binary, profile);
			}
			else
			{
				//Use the default directory
				webDriver = new FirefoxDriver();
			}
			
			webDriver.manage().window().maximize();
			
			//Set the maximum timeout to locate a web element
			webDriver.manage().timeouts().implicitlyWait(maxTimeout, TimeUnit.SECONDS);
			
			//Use function parameter for the url.
			webDriver.get(websiteURL);
		}
				
		//Create the IE Driver
		else if (browser.contains("ie"))
		{
			//Create the web driver.
			webDriver = createIEDriver(websiteURL);
			
			//Set the maximum timeout to locate a web element
			webDriver.manage().timeouts().implicitlyWait(maxTimeout, TimeUnit.SECONDS);
		}
				
		//Create safari driver
		else if (browser.contains("safari"))
		{
			//Add capability to accept certificates.  Am getting a "Safari can't verify the identity of the website"
			DesiredCapabilities capabilities = new DesiredCapabilities();
	        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

			webDriver = new SafariDriver(capabilities);
			webDriver.manage().window().maximize();
			
			//Set the maximum timeout to locate a web element
			webDriver.manage().timeouts().implicitlyWait(maxTimeout, TimeUnit.SECONDS);
			
			//Set the website URL.
			webDriver.get(websiteURL);
		}
				
		else
		{
			webDriver = null;
		}
	}
	
	
	//find web element by X path
	public static void ClickButton(String xpath)
	{
		//Use findElement and catch the not found exception.
		try
		{
			WebElement element = webDriver.findElement(By.xpath(xpath));
			element.click();
		}
		catch (WebDriverException e)
		{
		}
	}
	
	//Close the browser exe files depending on the web browser that was tested.
	public static void closeBrowserApps(String browser)
	{
		webDriver.quit();
		//Close the chrome driver apps
		if (browser.contains("chrome"))
		{
			try
			{
				//Close the Chrome browser
				killExecFile("chrome.exe");
				
				//Close the Chrome Driver Server
				killExecFile("chromedriver.exe");
			}
			catch(WindowsRegistryException e)
			{
				
			}
		}
										
		//Close the firefox driver apps
		else if (browser.contains("firefox"))
		{
			killExecFile("firefox.exe");
		}
		
		//Close the internet explorer driver apps
		else if (browser.contains("ie"))
		{
				
			try 
			{
				//Close the internet explorer app
				killExecFile("iexplore.exe");
				
				//Close the IE Driver Server
				killExecFile("IEDriverServer.exe");
			}
			catch(WindowsRegistryException e)
			{
				
			}
		}
		
		//Close the safari driver apps
		else if (browser.contains("safari"))
		{
			killExecFile("safari.exe");
		}
	}
	
	
	//Execute the window utilities "try to kill by name" function.  Use function to enclose call in try/catch block
	private static void killExecFile(String execName)
	{
		try
		{
			WindowsUtils.tryToKillByName(execName);
		}
		catch(WindowsRegistryException e)
		{	
		}
	}
	
	
	//Move the mouse to keep the computer from not going asleep.
	//Selenium tests don't keep computer alive.
    public void moveMouse()
	{
		Robot robot = null;
		try 
		{
			robot = new Robot();
		} 
		catch (AWTException e) 
		{
			e.printStackTrace();
		}
			
		robot.setAutoDelay(40);    
	    robot.setAutoWaitForIdle(true);        
	    robot.delay(1000);    
	    robot.mouseMove(40, 130);   
	    robot.delay(1000);
	    robot.mouseMove(40, 800);    
	    robot.delay(1000);     
	}

    /**
     * Using aapt command to get the package name, activity name and version name from apk file
     * @param AppPath
     * @return
     */
    public static String[] getAPKInfo(String AppPath)
	{
		String[] result=new String[3];
		int line=0;
		String[] commandResult=new String[2048];
		String Commands="aapt dump badging ";
		Commands+=AppPath+" | grep package";
		System.out.println(Commands);
		line=runWinCommnad(Commands,commandResult);
		for(int i=0;i<line;i++)
		{
			// Get the package name
			if(commandResult[i].indexOf("package:")!=-1)
			{
				String[] lineArray=commandResult[i].split("'");
				result[0]=lineArray[1];
			}
			// Get the launchable activity
			if(commandResult[i].indexOf("launchable-activity:")!=-1)
			{
				String[] lineArray=commandResult[i].split("'");
				
				/*if(lineArray[1].indexOf(result[0])!=-1)
				{
					result[1]=lineArray[1].replace(result[0], "");
				}
				else
				{*/
					result[1]=lineArray[1];
				//}	
			}
			// Get the current app version
			if(commandResult[i].indexOf("versionName")!=-1)
			{
				String[] lineArray=commandResult[i].split("versionName='");
				String[] Array=lineArray[1].split("'");
				result[2]=lineArray[0];
				AppSWVersion=Array[0].split("-")[0];
			}
		}
		return result;
	}
		
		
/*===============================================================================
	#Function Name       : commonInit()
	#Function Type       : Public Method
	#Description         : This method is to init configure the Appium
	#Parameters          : String DeviceName: The serial number of the test phone      
	#Return Value        : AppiumDriver: the Appium driver 
	#Programmer          : Bin Liu
	#Created Date        : 9/8/2015
	#Modified Date       :
	#Modification Reason :
===============================================================================*/

	public static SwipeableWebDriver commonInit(String DeviceName,String appPath,String appMode,String SystemVersion)
	{
		SwipeableWebDriver driver = null;
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName",DeviceName);
		capabilities.setCapability("udid",DeviceName);
		capabilities.setCapability("platformVersion", SystemVersion);
		capabilities.setCapability("appPackage",AppPackage);// the package of the test app
		capabilities.setCapability("appActivity",AppActivity);// main activity class
		capabilities.setCapability("platformName", "Android");// the test platform name "Android"
		capabilities.setCapability("app", appPath);// The path of the test application apk file
		
		if(appMode.equalsIgnoreCase("Hybrid") || appMode.equalsIgnoreCase("Web"))
		{
			capabilities.setCapability("automationName","Selendroid");
		}
		if(appMode.equalsIgnoreCase("Native"))
		{
			capabilities.setCapability("automationName","appium");
		}
		capabilities.setCapability("noReset", true);
		capabilities.setCapability("DeviceReadyTimeout", 15);
		capabilities.setCapability("newCommandTimeout", 180);
		// http://127.0.0.1:4723/wd/hub the AppiumServer address
		//capabilities.setCapability("app-wait-activity", "activity-to-wait-for");
		if ("".equals(StartActivity) == false)
		{
			capabilities.setCapability("appWaitActivity", StartActivity);//"com.ford.oa.dashboard.activities.DashboardActivity");
		}
		else
		{
			capabilities.setCapability("appWaitActivity", AppActivity);
		}
		//driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
		try {
			driver = new SwipeableWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try 
		{
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		VIEW_NAME=getViewName(driver);
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@The view name is:"+VIEW_NAME);
		//
		if(VIEW_NAME.indexOf("NATIVE_APP")!=-1)
			driver.context(VIEW_NAME);
		else
		{
			System.out.println("Switch the View");
			driver.context(VIEW_NAME);
		}
			//driver.switchTo().window(VIEW_NAME);
		delay(2000);
		// Change the Appium to Webview Mode to help find the web element
		return driver;
	}
	
	
	/**
	 *  Get the current app view context name
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String getViewName(WebDriver driver)
	{
		String ViewName = null;
		Set<String> contextNames = ((AppiumDriver) driver).getContextHandles();
		for (final String contextName : contextNames)
		{
			System.out.println("Context name is:"+contextName);
			if(contextName.indexOf("WEB")!=-1)
			{
				ViewName=contextName;
				break;
			}
			else
			{
				ViewName=contextName;
			}
		}
		return ViewName;
	}
	
/*===============================================================================
	#Function Name       : presenceOfElementLocated()
	#Function Type       : Public Method
	#Description         : This method is to check the object exits
	#Parameters          : By locator: the type for check the object       
	#Return Value        : AppiumDriver: the Appium driver 
	#Programmer          : Bin Liu
	#Created Date        : 9/21/2015
	#Modified Date       :
	#Modification Reason :
===============================================================================*/	
	public static Function<WebDriver,WebElement> presenceOfElementLocated(final By locator) {
        return new Function<WebDriver, WebElement>() {
            @Override
            public WebElement apply(WebDriver driver) {
                return driver.findElement(locator);
            }
        };
    }
	
	//12/17/15 Reads an EMAIL message in yahoo.com and returns the message text.
	//Parameters:
	//
	// address - email address
	// password - password
	// subjectText - subject line to search for in unread email inbox
	//
	// returns: email message text or error message beginning with "ERROR:"
	//
	public static  String readYahooEMAIL(String address, String password, String subjectText)
	{
		System.out.println("READ_EMAIL: bring up yahoo.com website");
		
		//Get the yahoo.com website.  browserType can be chrome.
		getWebDriver("https://sg.yahoo.com/","chrome",3000);
		
		//If we couldn't connect, return
		if (webDriver == null)
		{
			//close the browser and driver
			webDriver.quit();
			closeBrowserApps("chrome");
			return "ERROR: failed going to mail.com website";
		}
		
		
		//Find the sign in button//*[@id="yucs-profile"]/a/b
		WebElement signInBtn = findElemByXPath(webDriver,"//*[@id='yucs-profile']/a/b");
		
		if (signInBtn == null)
		{
			//close the browser and driver
			webDriver.quit();
			closeBrowserApps("chrome");
			return "ERROR: failed to find login button element";
		}
		
		//Click the login button
		signInBtn.click();
		delay(1000);
		
		
		//Wait for the email input box on the login page
		WebElement emailInput = findElemById(webDriver,"login-username");
		
		if (emailInput == null)
		{
			//close the browser and driver
			webDriver.quit();
			closeBrowserApps("chrome");
			return "ERROR: failed to find email input box";
		}

		//Enter the email address
		System.out.println("account is:"+address);
		emailInput.sendKeys(address);
		
		WebElement NextBtn=findElemById(webDriver,"login-signin");
		NextBtn.click();
		delay(5000);
		
		//Find the password input box on the login page
		WebElement passInput = findElemById(webDriver,"login-passwd");	
		
		if (passInput == null)
		{
			//close the browser and driver
			webDriver.quit();
			closeBrowserApps("chrome");
			return "ERROR: failed to find password input box";
		}
		
		System.out.println("password is:"+password);
		//Enter the password
		passInput.sendKeys(password);
		
		//Add return on the password input to advance to email page
		passInput.sendKeys(Keys.RETURN);
		delay(1000);
		
		//Click on Email button
		WebElement EmailBtn =  findElemByXPath(webDriver, "//*[@id='yucs-mail_link_id']/b[2]");
		
		if(EmailBtn==null)
		{
			//close the browser and driver
			webDriver.quit();
			closeBrowserApps("chrome");
			return "ERROR: failed to find email button";
		}
		
		EmailBtn.click();
		delay(8000);
		
		//Find the email elements by class name as subj
		List<WebElement> classElements =  findElementsByXPath(webDriver, "//*[@class='subj']/span[1]");
		
		//Get number of emails
		int size = classElements.size();
		
		System.out.println("the size is:"+size);
		
		//Check if we got emails
		if (classElements.isEmpty())
		{
			//close the browser and driver
			webDriver.quit();
			closeBrowserApps(browserType);
			return "ERROR: There are no emails";
		}
			
		
		//if true, email subject was found
		boolean tabFound = false;
		
		//Text of the email found.
		String mailText = null;
		WebElement classElem = null;
		String classText;
		int num=0;
		//list-view-items-page
		
		//Step through the elements to find the mail subject
		for (int i=0;i<size;i++)
		{
			classElem = classElements.get(i);
			classText = classElem.getText();
			num++;
			System.out.println("Text="+classText);
			
			//Check if the email subject was found
			if (classText.equals(subjectText))
			{
				tabFound = true;
				System.out.println("Find the subject!");
				break;
			}	
		}
		
		String xpath=String.format("//*[@class='list-view-items-page']/div[%d]/div[1]/div[2]/div[1]", num+2);
		classElem=findElemByXPath(webDriver, xpath);
		//Found the email subject
		if (tabFound)
		{
			//Click the email subject to open it.
			classElem.click();
			delay(4000);
			
			//Find the mail element
			WebElement mailElem = findElemByXPath(webDriver, "//*[@class='body undoreset']/div[1]/div[1]");
			if (mailElem == null)
			{
				//close the browser and driver
				webDriver.quit();
				closeBrowserApps("chrome");
				return "ERROR: Could not find the email element";
			}
			else
			{
				//Get the text of the email.
				delay(1000);
				mailText = mailElem.getText();
			}	
		}
		
		webDriver.quit();
		closeBrowserApps("chrome");
		
		return mailText;
	}
	
	/**
	 * Uinstall the application
	 */
	public static void UninstallApp(String DeviceSerialNum,String Package,String AppPath)
	{
		if(AppPath!=null)
		{
			String[] commandResult=new String[64];
			String Command="adb.exe -s ";
			Command=Command+DeviceSerialNum+" uninstall ";
			Command=Command+Package;
			System.out.println("Command is:"+Command);
			runWinCommnad(Command,commandResult);
		}
	}
	
	/**
	 * Uinstall the application
	 */
	public static void UninstallApp()
	{
		if(AppPackage != null && AndroidSerialNumber != null)
		{
			String[] commandResult=new String[64];
			String Command="adb.exe -s ";
			Command=Command+AndroidSerialNumber+" uninstall ";
			Command=Command+AppPackage;
			System.out.println("Command is:"+Command);
			runWinCommnad(Command,commandResult);
		}
	}
	
	
	/**
	 * Install the application
	 */
	public static void InstallApp(String DeviceSerialNum,String APKPath)
	{
		if(APKPath!=null)
		{
			int i=0;
			String[] commandResult=new String[64];
			String Command="adb.exe -s ";
			Command=Command+DeviceSerialNum+" install ";
			Command=Command+APKPath;
			System.out.println("Command is:"+Command);
			int line=runWinCommnad(Command,commandResult);
			if(line>0)
			{
				for(i=0;i<line;i++)
				{
					if(commandResult[i].contains("Success"))
					{
						break;
					}
					if(commandResult[i].contains("INSTALL_FAILED_ALREADY_EXISTS"))
					{
						JOptionPane.showConfirmDialog(null,"This App has already installed on this device, please uninstall it firstly!",
				           		"Install Error",
				           		JOptionPane.CLOSED_OPTION);
						break;
					}
				}
				if(i==line)
				{
					JOptionPane.showConfirmDialog(null,"Install failed, please check your device connection!",
			           		"Install Error",
			           		JOptionPane.CLOSED_OPTION);
				}
			}
		}
	}
	
	/**
	 * Install the application
	 */
	public static void InstallApp()
	{
		if(AppPath != null)
		{
			int i=0;
			String[] commandResult=new String[64];
			String Command="adb.exe -s ";
			Command=Command + AndroidSerialNumber + " install ";
			Command=Command + AppPath;
			System.out.println("Command is:"+Command);
			int line=runWinCommnad(Command,commandResult);
			if(line>0)
			{
				for(i=0;i<line;i++)
				{
					if(commandResult[i].contains("Success"))
					{
						break;
					}
					if(commandResult[i].contains("INSTALL_FAILED_ALREADY_EXISTS"))
					{
						JOptionPane.showConfirmDialog(null,"This App has already installed on this device, please uninstall it firstly!",
				           		"Install Error",
				           		JOptionPane.CLOSED_OPTION);
						break;
					}
				}
				if(i==line)
				{
					JOptionPane.showConfirmDialog(null,"Install failed, please check your device connection!",
			           		"Install Error",
			           		JOptionPane.CLOSED_OPTION);
				}
			}
		}
	}
	
	/**
	 * get the install application version
	 */
	public static String getAppVersion(String AppPackage, String DeviceSerialNum)
	{
		String InstallAppVersion = null;
		String[] commandResult=new String[64];
		String Command="adb.exe -s ";
		Command=Command+DeviceSerialNum+" shell dumpsys package ";
		Command=Command+AppPackage+" | grep versionName";
		System.out.println("Command is:"+Command);
		int line=runWinCommnad(Command,commandResult);
		
		if(line>0)
		{
			
			if(commandResult[0].indexOf("error")==-1 || commandResult[0].indexOf("Failure")==-1 || commandResult[0].indexOf("waiting")==-1)
			{
				String[] ResultArray=commandResult[0].split("=");
				InstallAppVersion=ResultArray[1];
			}
			else
			{
				//System.out.println("Here");
				if(commandResult[0].indexOf("error")!=-1 || commandResult[0].indexOf("waiting")!=-1)
					JOptionPane.showConfirmDialog(null,"Please check the Phone connection status",
		           		"Connection Error!", JOptionPane.CLOSED_OPTION);
				else
					InstallAppVersion=null;
			}
		}
		return InstallAppVersion;
	}
	
	/**
	 * Running windows command
	 */
	public static int runWinCommnad(String Commands,String[] commandResult)
	{
		Runtime rt = Runtime.getRuntime();
		Process proc = null;
		int line=0;
		try {
			proc = rt.exec(Commands);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}

		BufferedReader stdInput = new BufferedReader(new 
		     InputStreamReader(proc.getInputStream()));
		// read the output from the command
		String s = null;
		try {
			while ((s = stdInput.readLine()) != null || line==64) {
				commandResult[line]=s;
				line++;
			}
		} 
		
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}
		return line;
	}
	
/*===============================================================================
	#Function Name       : Swipe()
	#Function Type       : Public Method
	#Description         : This method is to drag and swipe the 
	#Parameters          : driver: The object Appium driver      
	#Return Value        : none
	#Programmer          : Bin Liu
	#Created Date        : 9/16/2015
	#Modified Date       :
	#Modification Reason :
===============================================================================*/
	public static void swipe(double StartX,double StartY,double EndX,double EndY)
	{
		//String command="adb -s FA36KS900443 shell input swipe 491 659 244 659";
		int ScreenWidth=TestDriver.manage().window().getSize().width;
		int ScreenHight=TestDriver.manage().window().getSize().height;
		String command="adb -s ";
		command=command+AndroidSerialNumber+" shell input swipe "+StartX*ScreenWidth+" "+StartY*ScreenHight+" "+EndX*ScreenWidth+" "+EndY*ScreenHight;
		System.out.println(command);
		String[] commandResult=new String[64];
		runWinCommnad(command,commandResult);
	}
	
	public void inputString(String input)
	{
		String command="adb -s ";
		command=command+AndroidSerialNumber+" shell input text "+input;
		System.out.println(command);
		String[] commandResult=new String[64];
		runWinCommnad(command,commandResult);
		command="adb -s ";
		command=command+AndroidSerialNumber+" shell input keyevent KEYCODE_ENTER";
		System.out.println(command);
		commandResult=new String[64];
		runWinCommnad(command,commandResult);
	}
	
	public static void clickBack()
	{
		//String command="adb -s FA36KS900443 shell input swipe 491 659 244 659";
		String command="adb -s ";
		command=command+AndroidSerialNumber+" shell input keyevent 111";
		System.out.println(command);
		String[] commandResult=new String[64];
		runWinCommnad(command,commandResult);
	}
	
	/**
	 * Add compile diagnostic listener
	 * @author BLIU52
	 *
	 */
	public static class MyDiagnosticListener implements DiagnosticListener<JavaFileObject>
    {
        public void report(Diagnostic<? extends JavaFileObject> diagnostic)
        {
 
            System.out.println("Line Number->" + diagnostic.getLineNumber());
            System.out.println("code->" + diagnostic.getCode());
            System.out.println("Message->"
                               + diagnostic.getMessage(Locale.ENGLISH));
            System.out.println("Source->" + diagnostic.getSource());
            System.out.println(" ");
        }
    }
	
	
	public static boolean compileFile(File sourceFile) throws IOException
	{
		//get system compiler:
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        MyDiagnosticListener c = new MyDiagnosticListener();
        
        
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(c,
              Locale.ENGLISH,
              null);
	      
	    fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays
	          .asList(new File("bin")));
	    // Compile the file
	    boolean success = compiler.getTask(null, fileManager, null, null, null,
	    fileManager.getJavaFileObjectsFromFiles(Arrays.asList(sourceFile)))
	          .call();
	    fileManager.close();
	    return success;
	}
	
	
	/**
	 * 
	 * Compile all test case code
	 * @throws Exception
	 */
	public static void complieTestCases()throws Exception {
		File folder = new File("src");
		File[] listOfFiles = folder.listFiles();
      
		for (int i = 0; i < listOfFiles.length; i++) 
		{
    	  if(listOfFiles[i].isDirectory())
    	  {
    		  if(!listOfFiles[i].getName().equals("GUITool") && !listOfFiles[i].getName().equals("org") )
    		  {
    			  System.out.println("Directory " + listOfFiles[i].getName());
    			  File[] listTestCases = listOfFiles[i].listFiles();
    			  for (int j = 0; j < listTestCases.length; j++) 
    		      {
			          if (listTestCases[j].isFile()) 
			          {
			        	  System.out.println("	File " + listTestCases[j].getAbsolutePath());
			        	  File sourceFile=listTestCases[j];
			        	  compileFile(sourceFile);
			          }
    		      }
    		  }
    	  }
		}
	}
	
	
	/**
	 * Get all specific files name array
	 * @param FolderPath
	 * @param FileFormat
	 * @return
	 */
	public static String[] getFilesDirectionList(String FolderPath,String FileFormat)
	{
		String[] FilesPathList=new String[64];
		File folder = new File(FolderPath);
		File[] listOfFiles = folder.listFiles();
		int num=0;
		for (int i = 0; i < listOfFiles.length; i++) 
		{
			if(listOfFiles[i].isFile())
	    	{
				if(listOfFiles[i].getName().indexOf(FileFormat)!=-1)
				{
					FilesPathList[num]=listOfFiles[i].getName();
					num++;
				}
	    	}
		}
		String[] resultList=new String[num];
		for(int i=0;i<num;i++)
		{
			resultList[i]=FilesPathList[i];
		}
		return resultList;
	}
	
	/**
	 * Get connected Android phone serial number
	 */
	public static String[] getSerialNumber()
	{
		int line=0;
		String[] commandResult=new String[64];
		String Commands="adb.exe devices";
		line=runWinCommnad(Commands,commandResult);
		if(line<2)
		{
			JOptionPane.showConfirmDialog(null,"Please Connect your Android Devices",
	           		"Select answer",
	           		JOptionPane.CLOSED_OPTION);
			return null;
		}
		else
		{
			String[] AndroidSNArray=new String[line-2];
			for(int i=0;i<line-2;i++)
			{
				String[] command=commandResult[i+1].split("	");
				AndroidSNArray[i]=command[0];
			}
			return AndroidSNArray;
		}
	}
	
	/**
	 * Compare App versin1 with app version2, if version 1 is great than version 2, return value great than 0, otherwise return -1
	 * @param AppaVersion1 format(X.X.XX)
	 * @param AppVersion2 format(X.X.XX)
	 * @return
	 */
	public static int appVersionCompare(String AppVersion1,String AppVersion2)
	{
		int result=-1;
		int version1=0;
		int version2=0;
		
		String[] VersionArray1=AppVersion1.split("\\.");
		//System.out.println(VersionArray1[0]+" "+VersionArray1[1]+" "+VersionArray1[2] );
		version1=Integer.parseInt(VersionArray1[0])*1000+Integer.parseInt(VersionArray1[1])*100+Integer.parseInt(VersionArray1[2]);
		String[] VersionArray2=AppVersion2.split("\\.");
		version2=Integer.parseInt(VersionArray2[0])*1000+Integer.parseInt(VersionArray2[1])*100+Integer.parseInt(VersionArray2[2]);
		
		if(version1>=version2)
			result=version1-version2;
		else
			result=-1;
		return result;
	}
	
	/**
	 * 
	 * @param statusStr: the status string need update on the GUI
	 */
	public synchronized static void updateLogText(final String statusStr)
    {
        if (com.ford.automation.gui.tcu.AutomationTool.display == null || com.ford.automation.gui.tcu.AutomationTool.display.isDisposed()) 
            return;
        com.ford.automation.gui.tcu.AutomationTool.display.asyncExec(new Runnable() {

            public void run() {
            	com.ford.automation.gui.tcu.AutomationTool.styledTextLog.append(statusStr);
            }
        });

    }
	
	/**
	 * 
	 * @param statusStr: the status string need update on the GUI
	 */
	public synchronized static void updateRunningTestCase(final String Group,final String Feature)
    {
        if (com.ford.automation.gui.tcu.AutomationTool.display == null || com.ford.automation.gui.tcu.AutomationTool.display.isDisposed()) 
            return;
        com.ford.automation.gui.tcu.AutomationTool.display.asyncExec(new Runnable() {

            public void run() {
            	com.ford.automation.gui.tcu.AutomationTool.textGroup.setText(Group);
            	com.ford.automation.gui.tcu.AutomationTool.textFeature.setText(Feature);
            }
        });

    }
	
	/**
	 * 
	 * @param statusStr: the status string need update on the GUI
	 */
	public synchronized static void updateAppiumLogText(final String statusStr)
    {
        if (com.ford.automation.gui.tcu.AutomationTool.display == null || com.ford.automation.gui.tcu.AutomationTool.display.isDisposed()) 
            return;
        com.ford.automation.gui.tcu.AutomationTool.display.asyncExec(new Runnable() {

            public void run() {
            	com.ford.automation.gui.tcu.AutomationTool.styledAppiumLog.append(statusStr);
            }
        });

    }
}
