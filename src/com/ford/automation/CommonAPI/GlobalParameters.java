package com.ford.automation.CommonAPI;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class GlobalParameters {
	/**
	 * Test parameter definition
	 */
	// The Test status flag
	public static boolean TestStatusFlag=true;
	
	// The object array
	public static String[][] ObjectMap;
	
	// Log file Buffer object
	public static List<String> LogInBuffer= new ArrayList<String>();
	
	// Excel Result Buffer object
	public static List<String> ExcelResultBuffer= new ArrayList<String>();
	
	// Test Times
	public static int TestTimes=1;
	
	// Test Case Number
	public static int TestCaseNum=1;
	
	// Test Group name 
	public static String TestGroupName;
	
	// Test cases name
	public static String TestCaseName;
	
	// Test Start time Stamp
	public static String TestStartTime;
	
	// Test End time Stamp
	public static String TestEndTime;
	
	// Test End time Stamp
	public static long DeltaTime;
	
	// The serial number of test device
	public static String AndroidSerialNumber;
	
	// The application path
	public static String AppPath;
	
	// The test plan Path
	public static String TestPlanPath;
	
	// The open test plan first time flag
	public static boolean FirstTimeOpenTestPlan = true;
	
	// The test phone version
	public static String SystemVersion;
	
	// The path of the object map 
	public static String OBJECT_MAP_PATH;
	
	// The sheet name of the object map
	public static String OBJECT_SHEET_NAME;
	
	// The version of the test app
	public static String AppSWVersion;
	
	// The package name of the test app
	public static String AppPackage;
	
	// The activity of the test app
	public static String AppActivity;
	
	// The current driver view name 
	public static String VIEW_NAME;
	
	// The app mode
	public static String APP_MODE;
	
	// The start activity name
	public static String StartActivity = null;
	
	// The Appium thread
	public static Thread AppiumThread = null;
	
	// Appium log printer
	public static PrintWriter AppiumLogPrinter = null;
	
	// Appium thread stop flag
	public volatile static boolean ThreadStop = false;
	
	// Stop test enable
	public static boolean StopEnable = true;
}
