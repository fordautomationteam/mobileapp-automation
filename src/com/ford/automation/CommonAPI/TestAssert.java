package com.ford.automation.CommonAPI;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class TestAssert extends SeleniumCommonAPI {
	
	public static void onTestFailure(String message)
	{	
		TestStatusFlag=false;
		//print the stack trace
		StringWriter sw = new StringWriter();
		String TestFailFirstLine;
		
		if(message!=null)
		{
			TestFailFirstLine=message+" at time: "+getCurrentTime("hh:mm");
		}
		else
		{
			TestFailFirstLine="Failed happened at time: "+getCurrentTime("hh:mm");
		}
		Throwable t = new Throwable(TestFailFirstLine);
		t.printStackTrace();
		t.printStackTrace(new PrintWriter(sw));
		LogInBuffer.add(sw.toString());
		
		
		//Create the png file name for the picture
		String fileName = "FailureScreenShots\\"+TestGroupName+"_"+TestCaseName+"_"+getCurrentTime("hh:mm");
		
		//Replace colons in the time with under-scores
	    String name1 = fileName.replace(":", "_");
	    
	    //Replace periods with _
	    String name2 = name1.replace("/", "_");
	    
	    name2 += ".png";
	 
	    File scrFile=null;
		
		if(WEB_ENABLE)
		{
			//Get the screen shot
			scrFile = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
		}
		else
		{
			//Get the screen shot
			
			if(TestDriver!=null)
			{
				System.out.println("Take the screen shot");
				scrFile = ((TakesScreenshot)TestDriver).getScreenshotAs(OutputType.FILE);
				int num=0;
				while(scrFile.length()/1024<3 && num <5)
				{
					scrFile = ((TakesScreenshot)TestDriver).getScreenshotAs(OutputType.FILE);
					num++;
				}
			}
		}
		
		
		
		//Copy the screen shot to the fileName
		try 
		{
			FileUtils.copyFile(scrFile, new File(name2));
		} 
		catch (IOException e) 
		{
			System.out.println("TAKE_SCREENSHOT:point #9"); 
		}
	
	}

	
	/**
	 * 
	 * @param actual
	 * @param expected
	 */
	public static void assertEquals(boolean actual,boolean expected,String message)
	{
		if(actual!=expected)
		{
			String printMessage;
			printMessage=message+" The expected is "+expected+", but the actual is "+actual;
			onTestFailure(printMessage);
			
			
		}
	}
	
	
	/**
	 * 
	 * @param actual
	 * @param expected
	 */
	public static void assertEquals(String actual,String expected,String message)
	{
		if(!actual.equals(expected))
		{
			String printMessage;
			printMessage=message+" The expected is "+expected+", but the actual is "+actual;
			System.out.println(printMessage);
			onTestFailure(printMessage);
		}
	}
	
	public static void assertFail()
	{
		onTestFailure(null);
	}
	
	public static void assertFail(String message)
	{
		onTestFailure(message);
	}
}
