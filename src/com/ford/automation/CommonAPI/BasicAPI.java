package com.ford.automation.CommonAPI;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.TimeoutException;

public class BasicAPI extends TestAssert {
	
	/**
	 * This function is used to init the test driver
	 */
	public static void InitTestDriver()
	{
		TestDriver=commonInit(AndroidSerialNumber,AppPath,APP_MODE,SystemVersion);
	}
	
	/**
	 * Set the driver view context 
	 * @param ViewName: Web or Native
	 */
	@SuppressWarnings({ })
	public static void setView(String ViewName)
	{
		if(VIEW_NAME!=null)
		{
			if(ViewName.equalsIgnoreCase("Web"))
			{
				if(VIEW_NAME.equalsIgnoreCase("WEBVIEW_0")==false)
				{
					TestDriver.switchTo().window("WEBVIEW_0");
					VIEW_NAME="WEBVIEW_0";
					delay(2000);
				}
			}
			if(ViewName.equalsIgnoreCase("Native"))
			{
				if(VIEW_NAME.equalsIgnoreCase("NATIVE_APP")==false)
				{
					TestDriver.switchTo().window("NATIVE_APP");
					VIEW_NAME="NATIVE_APP";
					delay(2000);
				}
			}
		}
		else
		{
			String CurrentView=getViewName(TestDriver);
			System.out.println("First Time set view to Native");
			TestDriver.switchTo().window(CurrentView);
			VIEW_NAME=CurrentView;
		}
		
	}
	
	/**
	 * Start the Test case and create the log files
	 * @param TestGroup: get test group from the test plan
	 * @param TestCase: copy the test case from the test plan
	 */
	public static void startTestCase(String TestGroup,String TestCase, String CaseCode)
	{
		if(TestStatusFlag==false)
		{
			TestStatusFlag=true;
		}
		if(ObjectMap==null)
		{
			ObjectMapInit(OBJECT_SHEET_NAME);
		}
		TestStartTime=getCurrentTimeWithSecond();
		TestGroupName=TestGroup;
		TestCaseName=TestCase;
		
		
		try 
		{
			updateRunningTestCase(TestGroupName,TestCaseName);
			String logMsg=TestCaseNum+"."+TestGroup+":"+TestCase;
			String line = null;
			String fileName = System.getProperty("user.dir")+"\\TestResults\\log.txt";
			File LogFile=new File(fileName);
			File dir = new File("TestResults");
		    dir.mkdir();
		    
		    dir = new File("FailureScreenShots");
		    dir.mkdir();
		    
		    
			if (LogFile.exists()) 
			{
				if(TestCaseNum==1)
					LogFile.delete();
				else
				{
					FileReader fr = new FileReader(LogFile);
		            BufferedReader br = new BufferedReader(fr);
		            while ((line = br.readLine()) != null) {
		                LogInBuffer.add(line);
		            }
		            
		            fr.close();
		            br.close();
				}
			}
			else
			{
				LogFile.createNewFile();
			}
			LogInBuffer.add(logMsg);
			updateLogText(logMsg+"\n");
			TestCaseNum++;
			
			logMsg = "Loading the test driver...";
			LogInBuffer.add(logMsg);
			updateLogText(logMsg+"\n");
			InitTestDriver();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Start the Test case and create the log files
	 * @param TestGroup: get test group from the test plan
	 * @param TestCase: copy the test case from the test plan
	 */
	public static void startTestCase(String TestGroup,String TestCase)
	{
		if(TestStatusFlag==false)
		{
			TestStatusFlag=true;
		}
		if(ObjectMap==null)
		{
			ObjectMapInit(OBJECT_SHEET_NAME);
		}
		TestStartTime=getCurrentTimeWithSecond();
		TestGroupName=TestGroup;
		TestCaseName=TestCase;
		
		InitTestDriver();
		try 
		{
			String logMsg=TestCaseNum+"."+TestGroup+":"+TestCase;
			String line = null;
			String fileName = System.getProperty("user.dir")+"\\TestResults\\log.txt";
			File LogFile=new File(fileName);
			File dir = new File("TestResults");
		    dir.mkdir();
		    
		    dir = new File("FailureScreenShots");
		    dir.mkdir();
		    
		    
			if (LogFile.exists()) 
			{
				if(TestCaseNum==1)
					LogFile.delete();
				else
				{
					FileReader fr = new FileReader(LogFile);
		            BufferedReader br = new BufferedReader(fr);
		            while ((line = br.readLine()) != null) {
		                LogInBuffer.add(line);
		            }
		            
		            fr.close();
		            br.close();
				}
			}
			else
			{
				LogFile.createNewFile();
			}
			LogInBuffer.add(logMsg);
			updateLogText(logMsg+"\n");
			TestCaseNum++;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Stop the test case 
	 * @param TestGroup: get test group from the test plan
	 * @param TestCase: copy the test case from the test plan
	 */
	public static void stopTestCase(String TestGroup,String TestCase, String CaseCode)
	{
		String TestResult;
		TestDriver.quit();
		TestEndTime = getCurrentTimeWithSecond();
		Date Start = null;
		Date End = null;
		SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss MM/dd/yy");
		try 
		{
			Start = format.parse(TestStartTime);
			End = format.parse(TestEndTime);
			long diff = End.getTime() - Start.getTime();
			DeltaTime = diff/1000;
		} 
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String ExcelResultStr = TestGroupName+","+TestCaseName+","+TestStatusFlag+","+TestStartTime+","+TestEndTime+","+DeltaTime;
		ExcelResultBuffer.add(ExcelResultStr);
		
		if(TestStatusFlag)
		{
			TestResult="Pass";
		}
		else
		{
			TestResult="Fail";
		}
		WriteResultIntoTestPlan WriteResult = new WriteResultIntoTestPlan(CaseCode,TestResult);
		try {
			WriteResult.modifyTestPlan();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(TestStatusFlag == false)
		{
			TestStatusFlag = true;
		}
	}
	
	/**
	 * Stop the test case 
	 * @param TestGroup: get test group from the test plan
	 * @param TestCase: copy the test case from the test plan
	 */
	public static void stopTestCase(String TestGroup,String TestCase)
	{
		TestDriver.quit();
		TestEndTime = getCurrentTimeWithSecond();
		Date Start = null;
		Date End = null;
		SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss MM/dd/yy");
		try 
		{
			Start = format.parse(TestStartTime);
			End = format.parse(TestEndTime);
			long diff = End.getTime() - Start.getTime();
			DeltaTime = diff/1000;
		} 
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String ExcelResultStr = TestGroupName+","+TestCaseName+","+TestStatusFlag+","+TestStartTime+","+TestEndTime+","+DeltaTime;
		ExcelResultBuffer.add(ExcelResultStr);
		
		if(TestStatusFlag == false)
		{
			TestStatusFlag = true;
		}
	}
	
	/**
	 * This function is used to stop the test, then it will save the log and generate the Excel report
	 */
	public static void stopTest()
	{
		if(StopEnable)
		{
			try 
			{
				// Save the programmer logging file
				String fileName = System.getProperty("user.dir")+"\\TestResults\\log.txt";
				File LogFile=new File(fileName);
				FileWriter fw = new FileWriter(LogFile);
	            BufferedWriter out = new BufferedWriter(fw);
	            StopEnable = false;
	            if(LogInBuffer!=null)
	            {
		            for(String s : LogInBuffer)
		            {
		                 out.write(s);
		                 out.newLine();
		            }
	            }
	            LogInBuffer.clear();
	            out.flush();
	            out.close();
	            
	            // Save the Excel result report
	            String ExcelFileName = System.getProperty("user.dir")+"\\TestResults\\ResultReport.xls";
	            HSSFWorkbook ResultWb = new HSSFWorkbook();
	            HSSFSheet TestResultSheet = ResultWb.createSheet(OBJECT_SHEET_NAME);
	            int RowNum=1;
	            
	            HSSFRow row = TestResultSheet.createRow(0);
				HSSFCell cell = row.createCell(0);
				cell.setCellValue("Test Group");
				setCellStyle(ResultWb,cell,"Orange",true);
				cell = row.createCell(1);
				cell.setCellValue("Test Case");
				setCellStyle(ResultWb,cell,"Orange",true);
				cell = row.createCell(2);
				cell.setCellValue("Result");
				setCellStyle(ResultWb,cell,"Orange",true);
				cell = row.createCell(3);
				cell.setCellValue("Start Time");
				setCellStyle(ResultWb,cell,"Orange",true);
				cell = row.createCell(4);
				cell.setCellValue("End Time");
				setCellStyle(ResultWb,cell,"Orange",true);
				cell = row.createCell(5);
				cell.setCellValue("Delta Time");
				setCellStyle(ResultWb,cell,"Orange",true);
				
				for(String s : ExcelResultBuffer)
	            {
					row = TestResultSheet.createRow(RowNum);
					String[] ResultArray=s.split(",");
					for(int i=0;i<ResultArray.length;i++)
					{
						cell = row.createCell(i);
						if( i==0 )
						{
							setCellStyle(ResultWb,cell,"Yellow",false);
							cell.setCellValue(ResultArray[i]);
						}
						if( i==1 )
						{
							setCellStyle(ResultWb,cell,"Blue",false);
							cell.setCellValue(ResultArray[i]);
						}
						if(i==2)
						{
							if(ResultArray[i].equals("true"))
							{
								setCellStyle(ResultWb,cell,"Green",false);
								cell.setCellValue("PASS");
							}
							else
							{
								setCellStyle(ResultWb,cell,"Red",false);
								cell.setCellValue("FAIL");
							}
						}
						if( i==3 )
						{
							setCellStyle(ResultWb,cell,"Rose",false);
							cell.setCellValue(ResultArray[i]);
						}
						if( i==4 )
						{
							setCellStyle(ResultWb,cell,"Pink",false);
							cell.setCellValue(ResultArray[i]);
						}
						if( i==5 )
						{
							setCellStyle(ResultWb,cell,"Light Green",false);
							cell.setCellValue(ResultArray[i]);
						}
					}
					RowNum++;
	            }
				
				for(int colNum = 0; colNum<row.getLastCellNum();colNum++)   
					ResultWb.getSheetAt(0).autoSizeColumn(colNum);
				FileOutputStream output_file = new FileOutputStream(new File(ExcelFileName));
		        //write changes
				ResultWb.write(output_file);
		        //close the stream
		        output_file.close();
		        ResultWb.close();
		        ThreadStop=true;
		        AppiumLogPrinter.close();
	            
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Set the cell style for excel report
	 * @param wb
	 * @param cell
	 * @param BackGroudColor
	 * @param BoldEnable
	 */
	@SuppressWarnings("static-access")
	public static void setCellStyle(HSSFWorkbook wb,HSSFCell cell,String BackGroudColor,boolean BoldEnable)
	{
		HSSFCellStyle CellStyle=wb.createCellStyle();
		HSSFFont hSSFFont = wb.createFont();
	
		if(BoldEnable)
		{
			hSSFFont.setBold(true);
		}
		
		CellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		if(BackGroudColor.equals("Green"))
		{
			CellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
		}
		if(BackGroudColor.equals("Red"))
		{
			CellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		}
		if(BackGroudColor.equals("Yellow"))
		{
			CellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		}
		if(BackGroudColor.equals("Blue"))
		{
			CellStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		}
		if(BackGroudColor.equals("Orange"))
		{
			CellStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
		}
		if(BackGroudColor.equals("Pink"))
		{
			CellStyle.setFillForegroundColor(IndexedColors.PINK.getIndex());
		}
		if(BackGroudColor.equals("Rose"))
		{
			CellStyle.setFillForegroundColor(IndexedColors.ROSE.getIndex());
		}
		if(BackGroudColor.equals("Light Green"))
		{
			CellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		}
		CellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);             
		CellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);            
		CellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);              
		CellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		CellStyle.setFillPattern(CellStyle.BORDER_THIN);
		CellStyle.setFont(hSSFFont);
		cell.setCellStyle(CellStyle);
	}
	
	/**
	 * Get the object array from excel file
	 * @param SheetName
	 */
	public static void ObjectMapInit(String SheetName)
	{ 
		String LogMsg="Start your Test at time: "+getCurrentTimeWithDate();;
		LogInBuffer.add(LogMsg);
		updateLogText(LogMsg+"\n");
		
		LogMsg="\tObjectMapInit is called with SheetName: "+SheetName+" at time stamp: "+getCurrentTimeWithDate();;
		LogInBuffer.add(LogMsg);
		updateLogText(LogMsg+"\n");
		File inputWorkbook = new File(OBJECT_MAP_PATH);
		Workbook ConfigWb = null;
		
		try 
		{
			ConfigWb = Workbook.getWorkbook(inputWorkbook);
			Sheet worksheet = ConfigWb.getSheet(SheetName);
			int MaxRow=worksheet.getRows();
			int MaxCol=worksheet.getColumns();
			
			String[][] MapArray=new String[MaxRow][MaxCol];
			
			Cell cell = worksheet.getCell(1,0);
			APP_MODE=cell.getContents();
			System.out.println("App mode="+APP_MODE);
			
			for(int i=2;i<MaxRow;i++)
			{
				for(int j=0;j<MaxCol;j++)
				{
					cell = worksheet.getCell(j,i);
					MapArray[i-2][j]=cell.getContents();
				}
			}
			ObjectMap=MapArray;
			
			LogMsg="\t\tUpload the Object map into memeory successfully!";
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
		} 
		catch (BiffException | IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			LogMsg="\t\tUpload the Object map excel file cannot be found!";
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
		}	
	}
	
	/**
	 * Get the Xpath from the Object map array
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static int searchXpathFromObjectMap(ElementObjectMapData objectData)
	{
		int rc = objectData.populateElementData(ObjectMap);
		return rc;
	}
	
	/**
	 * Find the source value for the specific element
	 * @param objectData
	 * @return
	 */
	public static WebElement findElement(ElementObjectMapData objectData)
	{
		WebElement Element=null;
		
		int rc = searchXpathFromObjectMap(objectData);
		if ( rc != 0 )
		{
			System.out.println("Couold not find the elemenet\r\n");
			String LogMsg="\t\tCould not find the elemenet";
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			return null;
		}
		String SourceType = objectData.getSrcType();
		String MultiEnable = objectData.getMultiEnable();
		String SourceValue = objectData.getSourceValue();
		int MultiID = objectData.getElementId();
		
		if(SourceType.equalsIgnoreCase("Source ID"))
		{
			if(objectExistsWithId(TestDriver,SourceValue))
			{
				if(MultiEnable.equalsIgnoreCase("NO"))
				{
					Element=findElemById(TestDriver,SourceValue);
					
				}
				if(MultiEnable.equalsIgnoreCase("YES"))
				{
					List<WebElement> ButtonList=findElemsById(TestDriver,SourceValue);
					if(ButtonList.size()>=MultiID)
						Element=ButtonList.get(MultiID);
					else
						Element=null;
				}
			}
		}
		
		if(SourceType.equalsIgnoreCase("Name"))
		{
			if(MultiEnable.equalsIgnoreCase("NO"))
			{
				Element=findElemByName(TestDriver,SourceValue);			
			}
			if(MultiEnable.equalsIgnoreCase("YES"))
			{
				List<WebElement> ButtonList=findElementsByName(TestDriver,SourceValue);
				if(ButtonList.size()>=MultiID)
					Element=ButtonList.get(MultiID);
				else
					Element=null;
			}
		}
		
		if(SourceType.equalsIgnoreCase("Xpath"))
		{
			if(objectExistsWithXpath(TestDriver,SourceValue))
			{
				
				if(MultiEnable.equalsIgnoreCase("NO"))
				{
					Element=findElemByXPath(TestDriver,SourceValue);
					
				}
				if(MultiEnable.equalsIgnoreCase("YES"))
				{
					List<WebElement> ButtonList=findElementsByXPath(TestDriver,SourceValue);
					Element=ButtonList.get(MultiID);
				}
			}
		}
		
		if(SourceType.equalsIgnoreCase("Class Name"))
		{
			if(objectExistsWithClassName(TestDriver,SourceValue))
			{
				
				if(MultiEnable.equalsIgnoreCase("NO"))
				{
					Element=findElemByClass(TestDriver,SourceValue);
					
				}
				if(MultiEnable.equalsIgnoreCase("YES"))
				{
					List<WebElement> ButtonList=findElementsByClass(TestDriver,SourceValue);
					Element=ButtonList.get(MultiID);
				}
			}
		}
		
		return Element;
	}
	
	/**
	 *  Click the specific button 
	 * @param PageName
	 * @param ObjectName
	 * @param TestEnable
	 */
	public static void compareObjClassName(String PageName,String ObjectName,String ExpectedClass,boolean TestEnable)
	{
		if(TestStatusFlag)
		{	
			String LogMsg="\tcompareObjClassName is called with PageName: "+PageName+" and ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement obj=findElement(elementObjData);
			LogMsg="\t\tThe Button SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			if(obj!=null)
			{
				String ActualClass = obj.getAttribute("class");
				System.out.println("The class name is:"+ActualClass);
				if(ActualClass.indexOf(ExpectedClass) != -1)
				{
					delay(2000);
					LogMsg="\t\tThe object class name displayed correctly!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
				}
				else
				{
					LogMsg="\t\tThe object class name displayed wrong!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					if(TestEnable) 
						assertFail(LogMsg);
				}
				
			}
			else
			{
				LogMsg="\t\tThe Object cannot found";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable) 
					assertFail(LogMsg);
			}
		}
	}
	
	
	/**
	 *  Click the specific button 
	 * @param PageName
	 * @param ObjectName
	 * @param TestEnable
	 */
	public static void buttonLongTimeClick(String PageName,String ObjectName,boolean TestEnable)
	{
		if(TestStatusFlag)
		{	
			String LogMsg="\tbuttonClick is called with PageName: "+PageName+" and ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement Button=findElement(elementObjData);
			
			LogMsg="\t\tThe Button source value = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			if(Button!=null)
			{
				TouchActions action = new TouchActions(TestDriver);
				action.longPress(Button).perform();
				delay(5000);
				LogMsg="\t\tThe Button Click successfully!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}	
			else
			{
				LogMsg="\t\tThe Button cannot found";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The Button "+ObjectName+" cannot be found!");
			}	
		}
	}
	
	/**
	 *  Click the specific button 
	 * @param PageName
	 * @param ObjectName
	 * @param TestEnable
	 */
	public static void buttonClick(String PageName,String ObjectName,boolean TestEnable)
	{
		if(TestStatusFlag)
		{	
			String LogMsg="\tbuttonClick is called with PageName: "+PageName+" and ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement Button=findElement(elementObjData);
			LogMsg="\t\tThe Button SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			if(Button!=null)
			{
				Button.click();
				delay(2000);
				LogMsg="\t\tThe Button Click successfully!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			else
			{
				LogMsg="\t\tThe Button cannot found";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable) 
					assertFail("The Button "+ObjectName+" cannot be found!");
			}
		}
	}
	
	
	/**
	 * Check the Button whether exits
	 * @param PageName
	 * @param ObjectName
	 * @param TestEnable
	 * @return
	 */
	public static boolean buttonExits(String PageName,String ObjectName,boolean TestEnable)
	{
		boolean found=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tbuttonExits is called with PageName: "+PageName+" and ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();;
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement Button=findElement(elementObjData);
			LogMsg="\t\tThe Button SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			if(Button!=null)
			{
			
				found=true;
				LogMsg="\t\tThe Button dislplay!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			else
			{
				LogMsg="\t\tThe Button cannot be found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				found=false;
				if(TestEnable)
				{
					assertFail("The button cannot found!");
				}
			}
		}
		return found;
	}
	
	/**
	 * Check the Button whether exits in TimeOut
	 * @param PageName
	 * @param ObjectName
	 * @param TestEnable
	 * @return
	 */
	public static boolean buttonExits(String PageName,String ObjectName, int TimeOut,boolean TestEnable)
	{
		boolean found=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tbuttonExits is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" and Timeout:"+TimeOut+" at time stamp: "+getCurrentTimeWithDate();;
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement Button=findElement(elementObjData);
			LogMsg="\t\tThe Button SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			long StartTime=System.currentTimeMillis();
			long EndTime=0;
			float Duration=0;
			while(Duration<TimeOut)
			{
				EndTime=System.currentTimeMillis();
				Duration=(EndTime-StartTime)/1000;
				Button=findElement(elementObjData);
				
				if(Button!=null)
				{
					found=true;
					LogMsg="\t\tThe Button dislplay!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					break;
				}
				
			}
			if(found==false)
			{
				LogMsg="\t\tThe Button cannot be found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
				{
					assertFail("The button cannot found!");
				}
			}
		}
		
		return found;
	}
	
	
	/**
	 * Check the Button whether not exits in TimeOut
	 * @param PageName
	 * @param ObjectName
	 * @param TestEnable
	 * @return
	 */
	public static boolean buttonNotExits(String PageName,String ObjectName, int TimeOut,boolean TestEnable)
	{
		boolean found=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tbuttonNotExits is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" and Timeout:"+TimeOut+" at time stamp: "+getCurrentTimeWithDate();;
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement Button=findElement(elementObjData);
			LogMsg="\t\tThe Button SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			long StartTime=System.currentTimeMillis();
			long EndTime=0;
			float Duration=0;
			while(Duration<TimeOut)
			{
				EndTime=System.currentTimeMillis();
				Duration=(EndTime-StartTime)/1000;
				Button=findElement(elementObjData);
				if(Button!=null)
				{
					found=false;
					LogMsg="\t\tThe Button dislplay!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					break;
				}
			}
			if(Duration>=TimeOut)
			{
				found=true;
				LogMsg="\t\tThe Button not displayed!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			if(found==false)
			{
				LogMsg="\t\tThe Button displayed!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
				{
					if(TestEnable)
						assertFail("The button displayed!");
				}
			}
		}
		
		return found;
	}
	
	/**
	 * Get the text from the Button
	 * @param PageName
	 * @param ObjectName
	 * @param TestEnable
	 * @return
	 */
	public static String buttonGetText(String PageName,String ObjectName,boolean TestEnable)
	{
		String Text=null;
		if(TestStatusFlag)
		{
			String LogMsg="\tbuttonGetText is called with PageName: "+PageName+" and ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			
			WebElement Button=findElement(elementObjData);
			LogMsg="\t\tThe Button SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			if(Button!=null)
			{
				Text=Button.getText();
				delay(1000);
				LogMsg="\t\tThe Button text is: "+Text;
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			else
			{
				LogMsg="\t\tThe Button cannot be found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The Button "+ObjectName+" cannot found!");
			}
		}
		return Text;
	}
	
	/**
	 * Compare the text on the button with expected value
	 * @param PageName
	 * @param ObjectName
	 * @param Expected
	 * @param TestEnable
	 * @return
	 */
	public static boolean buttonCompareText(String PageName,String ObjectName,String Expected,boolean TestEnable)
	{
		boolean CompareResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tbuttonCompareText is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" and Expected String:"+Expected+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement Button=findElement(elementObjData);
			LogMsg="\t\tThe Button SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			if(Button!=null)
			{
				String Actual=Button.getText();
				if(Actual.equals(Expected))
				{
					CompareResult=true;
				}
				else
				{
					LogMsg="\t\tThe Button text value display error! the expected string is:"+Expected+" but actual string is:"+Actual;
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					if(TestEnable)
						assertFail("The Button "+ObjectName+" text displayed error! The actual displayed:"+Actual+" but the expected "
							+ "text is:"+Expected);
					CompareResult=false;
				}
				LogMsg="\t\tThe Button text value display correct!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				delay(2000);
			}
			else
			{
				LogMsg="\t\tThe Button cannot be found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The Button "+ObjectName+" cannot found!");
			}	
		}
		return CompareResult;
	}
	
	
	/**
	 * Click button in timeout when the button displayed
	 * @param PageName
	 * @param ObjectName
	 * @param Expected
	 * @param TimeOut
	 * @param TestEnable
	 * @return
	 */
	public static boolean buttonClick(String PageName,String ObjectName,String Expected,int TimeOut,boolean TestEnable)
	{
		boolean CompareResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tbuttonClick is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" and TimeOut:"+TimeOut+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement Button=findElement(elementObjData);
			LogMsg="\t\tThe Button SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			long StartTime=System.currentTimeMillis();
			long EndTime=0;
			float Duration=0;
			while(true)
			{
				EndTime=System.currentTimeMillis();
				Duration=(EndTime-StartTime)/1000;
				Button=findElement(elementObjData);
				System.out.println("The Duration="+Duration+" Timeout="+TimeOut);
				if(Button!=null)
				{
					System.out.println("Button is not null");
					if(Button.getText().indexOf(Expected)!=-1)
					{
						Button.click();
						CompareResult=true;
						break;
					}					
				}
				else
				{
					System.out.println("Button is null");
				}
				if(Duration>=TimeOut)
				{
					CompareResult=false;
					break;
				}
			}
			if(CompareResult==false)
			{
				LogMsg="\t\tThe Button text didn't display the expected button is:"+ObjectName+" in duration time:"+TimeOut;
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("Wait for the Button "+ObjectName+" display time out!");
			}
		}
		return CompareResult;
	}
	
	/**
	 * Compare the text on the button with expected value in timeout
	 * @param PageName
	 * @param ObjectName
	 * @param Expected
	 * @param TestEnable
	 * @return
	 */
	public static boolean buttonCompareText(String PageName,String ObjectName,String Expected,int TimeOut,boolean TestEnable)
	{
		boolean CompareResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tbuttonCompareText is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+", Expected String:"+Expected+" and TimeOut:"+TimeOut+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement Button=findElement(elementObjData);
			LogMsg="\t\tThe Button SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			
			long StartTime=System.currentTimeMillis();
			long EndTime=0;
			float Duration=0;
			while(true)
			{
				EndTime=System.currentTimeMillis();
				Duration=(EndTime-StartTime)/1000;
				Button=findElement(elementObjData);
				if(Button!=null)
				{
					String Actual=Button.getText();
					if(Actual.equals(Expected))
					{
						CompareResult=true;
						LogMsg="\t\tThe Button text display correctly, text is:"+Actual;
						LogInBuffer.add(LogMsg);
						updateLogText(LogMsg+"\n");
					}
					else
					{
						LogMsg="\t\tThe Button text value display error! the expected string is:"+Expected+" but actual string is:"+Actual;
						LogInBuffer.add(LogMsg);
						updateLogText(LogMsg+"\n");
						if(TestEnable)
							assertFail("The Button "+ObjectName+" text displayed error! The actual displayed:"+Actual+" but the expected "
								+ "text is:"+Expected);
						CompareResult=false;
					}
					break;
				}
				
				if(Duration>=TimeOut)
				{
					CompareResult=false;
					LogMsg="\t\tThe Button text didn't display the expected string is:"+Expected+" in duration time:"+TimeOut;
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					if(TestEnable)
						assertFail("Wait for the Button "+ObjectName+" display expect text time out!");
					break;
				}
			}
		
		}
		return CompareResult;
	}


	/**
	 * Check the checkBox whether exits
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static boolean checkBoxExits(String PageName,String ObjectName,boolean TestEnable)
	{
		boolean FindResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tcheckBoxExits is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement checkBox=findElement(elementObjData);
			LogMsg="\t\tThe Checkbox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			
			if(checkBox!=null)
			{
				FindResult=true;
				LogMsg="\t\tThe CheckBox found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			else
			{
				LogMsg="\t\tThe CheckBox:" +ObjectName+" cannot found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				FindResult=false;
				if(TestEnable)
				{
					assertFail("The CheckBox "+ObjectName+" cannot found!");
				}
			}
		}
		return FindResult;
	}
	
	/**
	 * Check the checkBox
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static boolean checkBoxCheck(String PageName,String ObjectName,boolean TestEnable)
	{
		boolean FindResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tcheckBoxCheck is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			

			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement checkBox =findElement(elementObjData);
			
			LogMsg="\t\tThe Checkbox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			
			if(checkBox!=null)
			{
				checkBox.click();
				if ( !checkBox.isSelected() )
				{
					checkBox.click();
				}
				LogMsg="\t\tThe CheckBox is checked successfully!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				delay(2000);
			}
			else
			{
				LogMsg="\t\tThe CheckBox:" +ObjectName+" cannot found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The CheckBox "+ObjectName+" cannot found!");
				FindResult=false;
			}
		}
		return FindResult;
	}
	
	/**
	 * Uncheck the checkBox
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static boolean checkBoxUncheck(String PageName,String ObjectName,boolean TestEnable)
	{
		boolean FindResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tcheckBoxUncheck is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");

			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement checkBox=findElement(elementObjData);
			LogMsg="\t\tThe Checkbox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			if(checkBox!=null)
			{
				if ( checkBox.isSelected() )
				{
					checkBox.click();
				}
				LogMsg="\t\tThe CheckBox is unchecked successfully!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				delay(2000);
			}
			else
			{
				LogMsg="\t\tThe CheckBox:" +ObjectName+" cannot found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The CheckBox "+ObjectName+" cannot found!");
				FindResult=false;
			}
		}
		return FindResult;
	}
	
	/**
	 * Check the checkBox status
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static boolean checkBoxCheckStatus(String PageName,String ObjectName,boolean TestEnable)
	{
		boolean FindResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tcheckBoxUncheck is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");

			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement checkBox=findElement(elementObjData);
			LogMsg="\t\tThe Checkbox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			if(checkBox!=null)
			{
				System.out.println(checkBox.getAttribute("checked"));
				if ( checkBox.getAttribute("checked").indexOf("true") != -1)
				{
					LogMsg="\t\tThe CheckBox is checked!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					delay(2000);
					FindResult = true;
				}
				else
				{
					LogMsg="\t\tThe CheckBox isn't checked!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					delay(2000);
					FindResult = false;
				}
				
			}
			else
			{
				LogMsg="\t\tThe CheckBox:" +ObjectName+" cannot found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The CheckBox "+ObjectName+" cannot found!");
				FindResult=false;
			}
		}
		return FindResult;
	}
	
	/**
	 * Check the textBox whether exits
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static boolean textBoxExits(String PageName,String ObjectName,boolean TestEnable)
	{
		boolean FindResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\ttextBoxExits is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTime("hh:mm");
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement textBox=findElement(elementObjData);
			LogMsg="\t\tThe textBox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			if(textBox!=null)
			{
				FindResult=true;
				LogMsg="\t\tThe TextBox found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			else
			{
				LogMsg="\t\tThe textBox:" +ObjectName+" cannot found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				FindResult=false;
				if(TestEnable)
				{
					assertFail("The textBox "+ObjectName+" cannot found!");
				}
			}
		}
		return FindResult;
	}
	
	/**
	 * Get the text from the textBox
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static String textBoxGetText(String PageName,String ObjectName,boolean TestEnable)
	{
		String Text=null;
		if(TestStatusFlag)
		{
			String LogMsg="\ttextBoxGetText is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement textBox=findElement(elementObjData);
			LogMsg="\t\tThe textBox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			if(textBox!=null)
			{
				Text=textBox.getText();
				delay(2000);
				LogMsg="\t\tThe textBox:" +ObjectName+" text string is:"+Text;
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			else
			{
				LogMsg="\t\tThe textBox:" +ObjectName+" cannot found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The textBox "+ObjectName+" cannot found!");
			}
		}
		return Text;
	}
	
	/**
	 * Compare the text on the textBox with expected value
	 * @param PageName
	 * @param ObjectName
	 * @param Expected
	 * @return
	 */
	public static boolean textBoxCompareText(String PageName,String ObjectName,String Expected,boolean TestEnable)
	{
		boolean CompareResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\ttextBoxCompareText is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" and Expected:"+Expected+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement textBox=findElement(elementObjData);
			LogMsg="\t\tThe textBox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			if(textBox!=null)
			{
				String Actual=textBox.getText();
				if(Actual.indexOf(Expected)!=-1)
				{
					CompareResult=true;
					LogMsg="\t\tThe textBox:" +ObjectName+" text string is:"+Actual;
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
				}
				else
				{
					LogMsg="\t\tThe textBox "+ObjectName+" text displayed error! The actual displayed:"+Actual+" but the expected "
							+ "text is:"+Expected;
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					if(TestEnable)
						assertFail("The textBox "+ObjectName+" text displayed error! The actual displayed:"+Actual+" but the expected "
							+ "text is:"+Expected);
					CompareResult=false;
				}
				delay(2000);
			}
			else
			{
				if(TestEnable)
					assertFail("The textBox "+ObjectName+" cannot found!");
				CompareResult=false;
			}
		}
		return CompareResult;
	}
	
	/**
	 * Compare the text on the textBox with expected value in timeout
	 * @param PageName
	 * @param ObjectName
	 * @param Expected
	 * @return
	 */
	public static boolean textBoxCompareText(String PageName,String ObjectName,String Expected,int TimeOut,boolean TestEnable)
	{
		boolean CompareResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\ttextBoxCompareText is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+", Expected:"+Expected+" and TimeOut:"+TimeOut+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement textBox=findElement(elementObjData);
			LogMsg="\t\tThe textBox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
				
			long StartTime=System.currentTimeMillis();
			long EndTime=0;
			float Duration=0;
			while(true)
			{
				EndTime=System.currentTimeMillis();
				Duration=(EndTime-StartTime)/1000;
				textBox=findElement(elementObjData);
				if(textBox!=null)
				{
					String Actual=textBox.getText();
					if(Actual.indexOf(Expected)!=-1)
					{
						CompareResult=true;
						LogMsg="\t\tThe textBox:" +ObjectName+" text string is:"+Actual;
						LogInBuffer.add(LogMsg);
						updateLogText(LogMsg+"\n");
						break;
					}
					else
					{
						LogMsg="\t\tThe textBox "+ObjectName+" text displayed error! The actual displayed:"+Actual+" but the expected "
								+ "text is:"+Expected;
						CompareResult=false;
					}
				}
				if(Duration>=TimeOut)
				{
					LogMsg="\t\tThe textBox "+ObjectName+" didn't display in duration:"+TimeOut+" display expect text time out!";
					updateLogText(LogMsg+"\n");
					CompareResult = false;
					break;
				}
			}
			
			if(CompareResult == false)
			{
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail(LogMsg);
			}
			delay(2000);
		}
		return CompareResult;
	}
	
	
	
	/**
	 * Check the inputBox whether exits
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static boolean inputBoxExits(String PageName,String ObjectName,boolean TestEnable)
	{
		boolean FindResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tinputBoxExits is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement inputBox=findElement(elementObjData);
			LogMsg="\t\tThe inputBox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			if(inputBox!=null)
			{
				FindResult=true;
			}
			else
			{
				LogMsg="\t\tThe inputBox "+ObjectName+" not found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				FindResult=false;
				if(TestEnable)
				{
					assertFail("The inputBox "+ObjectName+" cannot found!");
				}
			}
		}
		return FindResult;
	}
	
	/**
	 * Get the text from the Button
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static String inputBoxGetText(String PageName,String ObjectName,boolean TestEnable)
	{
		String Text=null;
		if(TestStatusFlag)
		{
			String LogMsg="\tinputBoxGetText is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
						
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement inputBox=findElement(elementObjData);
			LogMsg="\t\tThe inputBox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			if(inputBox!=null)
			{
				Text=inputBox.getAttribute("value");
				delay(2000);
				LogMsg="\t\tThe inputBox "+ObjectName+" value is:"+Text;
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			else
			{
				LogMsg="\t\tThe inputBox "+ObjectName+" not found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The inputBox "+ObjectName+" cannot found!");
			}		
		}
		return Text;
	}
	
	/**
	 * Compare the text on the inputBox with expected value
	 * @param PageName
	 * @param ObjectName
	 * @param Expected
	 * @return
	 */
	public static boolean inputBoxCompareText(String PageName,String ObjectName,String Expected,boolean TestEnable)
	{
		boolean CompareResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tinputBoxCompareText is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" and Expected String:"+Expected+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement inputBox=findElement(elementObjData);
			LogMsg="\t\tThe inputBox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			if(inputBox!=null)
			{
				String Actual=inputBox.getAttribute("value");
				if(Actual.equals(Expected))
				{
					CompareResult=true;
					LogMsg="\t\tThe inputBox has correct value: "+Expected;
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
				}
				else
				{
					LogMsg="\t\tThe inputBox "+ObjectName+" text displayed error! The actual displayed:"+Actual+" but the expected "
							+ "text is:"+Expected;
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					if(TestEnable)
						assertFail("The inputBox "+ObjectName+" text displayed error! The actual displayed:"+Actual+" but the expected "
							+ "text is:"+Expected);
					CompareResult=false;
				}
				delay(2000);
			}
			else
			{
				LogMsg="\t\tThe inputBox "+ObjectName+" not found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The inputBox "+ObjectName+" cannot found!");
				CompareResult=false;
			}
		}
		return CompareResult;
	}
	
	/**
	 * Compare the text on the inputBox with expected value in timeout
	 * @param PageName
	 * @param ObjectName
	 * @param Expected
	 * @return
	 */
	public static boolean inputBoxCompareText(String PageName,String ObjectName,String Expected,int TimeOut,boolean TestEnable)
	{
		boolean CompareResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tinputBoxCompareText is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+", Expected String:"+Expected+" and TimeOut:"+TimeOut+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement inputBox=findElement(elementObjData);
			LogMsg="\t\tThe inputBox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			long StartTime=System.currentTimeMillis();
			long EndTime=0;
			float Duration=0;
			while(true)
			{
				EndTime=System.currentTimeMillis();
				Duration=(EndTime-StartTime)/1000;
				inputBox=findElement(elementObjData);
				if(inputBox!=null)
				{
					String Actual=inputBox.getAttribute("value");
					if(Actual.equals(Expected))
					{
						CompareResult=true;
						LogMsg="\t\tThe inputBox with value found!";
						LogInBuffer.add(LogMsg);
						updateLogText(LogMsg+"\n");
						break;
					}
					else
					{
						LogMsg="\t\tThe inputBox "+ObjectName+" text displayed error! The actual displayed:"+Actual+" but the expected "
								+ "text is:"+Expected;
						LogInBuffer.add(LogMsg);
						updateLogText(LogMsg+"\n");
						if(TestEnable)
							assertFail("The inputBox "+ObjectName+" text displayed error! The actual displayed:"+Actual+" but the expected "
								+ "text is:"+Expected);
						CompareResult=false;
					}
				}
				if(Duration>=TimeOut)
				{
					CompareResult=false;
					LogMsg="\t\tThe inputBox not display with vlaue in duration time:"+TimeOut;
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					if(TestEnable)
						assertFail("Wait for the textBox "+ObjectName+" display expect text time out!");
					break;
				}
			}
		}
		return CompareResult;
	}
	
	/**
	 * 
	 * Write string to the inputBox
	 * @param PageName
	 * @param ObjectName
	 * @param WriteStr
	 * @return
	 */
	public static boolean inputBoxWrite(String PageName,String ObjectName,String WriteStr,boolean TestEnable)
	{
		boolean result=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tinputBoxWrite is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" and WritStr: "+WriteStr+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement inputBox=findElement(elementObjData);
			LogMsg="\t\tThe inputBox SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			System.out.println(LogMsg);
			if(inputBox!=null)
			{
				if(elementObjData.getViewMode().equalsIgnoreCase("web") && inputBox.getAttribute("value").length()>0)
				{
					System.out.println("here clearing:"+WriteStr);
					inputBox.clear();
					delay(2000);
					inputBox.clear();
					delay(2000);
				}
				inputBox.clear();
				delay(2000);
				inputBox.sendKeys(WriteStr);
				delay(2000);
				LogMsg="\t\tThe Input Box writting succesfully!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			else
			{
				LogMsg="\t\tThe Input Box cannot be found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The inputBox "+ObjectName+" cannot found!");
			}
		}
		return result;
	}
	 
	
	/**
	 * Check the combo list whether exits
	 * @param PageName
	 * @param ObjectName
	 * @return
	 */
	public static boolean comboListExits(String PageName,String ObjectName,boolean TestEnable)
	{
		boolean FindResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tcomboListExits is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement comboList=findElement(elementObjData);
			
			LogMsg="\t\tThe comboList SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			if(comboList!=null)
			{
				LogMsg="\t\tThe comboList found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				FindResult=true;
			}
			else
			{
				LogMsg="\t\tThe comboList "+ObjectName+" not found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				FindResult=false;
				if(TestEnable)
				{
					assertFail("The comboList "+ObjectName+" cannot found!");
				}
			}
		}
		return FindResult;
	}
	
	/**
	 * Choose the goal choice in the combo list
	 * @param PageName
	 * @param ObjectName
	 * @param GoalChoose
	 * @return
	 */
	public static boolean comboListChoose(String PageName,String ObjectName,String GoalChoice,boolean TestEnable)
	{
		boolean ChooseResult=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tcomboListExits is called with PageName: "+PageName+", ObjectName:"+ObjectName
					+" and the goal choice:"+GoalChoice+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			WebElement comboList=findElement(elementObjData);
			LogMsg="\t\tThe comboList SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			if(comboList!=null)
			{
				comboList.click();
				delay(3000);
				Select dropDown =new Select(comboList);
				List<WebElement> Options = dropDown.getOptions();
				boolean find=false;
				for(WebElement option:Options){
				    if(option.getText().equals(GoalChoice)) {
				      option.click(); 
				      find=true;
				      break;
				    }   
				}
				if(find)
				{
					LogMsg="\t\tThe comboList found and chose the goal choice successfully!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					ChooseResult=true;
				}
				else
				{
					LogMsg="\t\tThe comboList "+ObjectName+" option: "+GoalChoice+" not found!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					if(TestEnable)
						assertFail("The comboList "+ObjectName+" option: "+GoalChoice+" not found!");
					ChooseResult=false;
				}
				delay(2000);
			}
			else
			{
				LogMsg="\t\tThe comboList "+ObjectName+" not found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The comboList "+ObjectName+" cannot found!");
				ChooseResult=false;
			}
		}
		return ChooseResult;
	}
	
	
	/**
	 * Check the test exist in Xpath list with only change one tag number
	 * @param PageName
	 * @param ObjectName
	 * @param GoalText
	 * @return
	 */
	public static boolean clickListByText(String PageName,String ObjectName,String GoalText,boolean TestEnable)
	{
		boolean findFlag=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tcomboListExits is called with PageName: "+PageName+", ObjectName:"+ObjectName+" and GoalText:"+GoalText
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			System.out.println(LogMsg);
			
			WebElement listElement=null;
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			int rc = searchXpathFromObjectMap(elementObjData);
			if ( rc != 0 )
			{
				System.out.println("Couold not find the elemenet\r\n");
				LogMsg="\t\tCouold not find the elemenet";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			String SourceValue=elementObjData.getSourceValue();
			String SourceType=elementObjData.getSrcType();
			String MultiEnable=elementObjData.getMultiEnable();
			
			System.out.println("SourceValue="+SourceValue+"   SourceType="+SourceType);
			
			if(SourceType.equalsIgnoreCase("Source ID"))
			{
				if(objectExistsWithId(TestDriver,SourceValue))
				{
					if(MultiEnable.equalsIgnoreCase("NO"))
					{
						listElement=findElemById(TestDriver,SourceValue);
						listElement.click();
					}
					if(MultiEnable.equalsIgnoreCase("YES"))
					{
						int num=0;
						String CurrentVehicle=null;
						while(num<30)
						{	
							List<WebElement> ButtonList=findElemsById(TestDriver,SourceValue);
							int listSize=ButtonList.size();
							for(int i=0;i<listSize;i++)
							{
								listElement=ButtonList.get(i);
								CurrentVehicle=listElement.getText();
								if(CurrentVehicle.equalsIgnoreCase(GoalText))
								{
									listElement.click();
									findFlag=true;
									break;
								}
							}
							if(findFlag==false)
							{
								/*TouchActions swipe = new TouchActions(TestDriver).flick(listElement, 0, -300, 0);
						    	swipe.perform();*/
								swipe(0.5,0.75,0.5,0.5);
						    	ButtonList=findElemsById(TestDriver,SourceValue);
						    	listElement=ButtonList.get(0);
						    	CurrentVehicle=listElement.getText();
							}
							else
							{
								break;
							}
								
							num++;
						}
					}
				}
			}
			
			if(SourceType.equalsIgnoreCase("Xpath"))
			{
				if(MultiEnable.equalsIgnoreCase("YES"))
				{
					int num=1;
					String Xpath=String.format(SourceValue, 1);
					LogMsg="\t\tThe comboList Xpath = "+Xpath;
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					while(objectExistsWithXpath(TestDriver,Xpath))
					{
						if(objectExistsWithText(TestDriver,Xpath,GoalText))
						{
							findFlag=true;
							listElement=findElemByXPath(TestDriver,Xpath);
							listElement.click();
							break;
						}
						else
						{
							findFlag=false;
						}
						num++;
						Xpath=String.format(SourceValue, num);
					}
					
				}
			}
			if(findFlag)
			{
				LogMsg="\t\tThe text list found and chose the goal choice successfully!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			else
			{
				LogMsg="\t\tThe GoalText list "+GoalText+" cannot found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The GoalText list "+GoalText+" cannot found!");
			}
		}
		return findFlag;
	}
	
	
	/**
	 * Check the test exist in Xpath list with only change one tag number
	 * @param PageName
	 * @param ObjectName
	 * @param GoalText
	 * @return
	 */
	public static boolean checkListTextExits(String PageName,String ObjectName,String GoalText,boolean TestEnable)
	{
		boolean findFlag=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tcheckListTextExits is called with PageName: "+PageName+", PbjectName:"+ObjectName+", GoalText:"+GoalText
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			int num=1;
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			int rc = searchXpathFromObjectMap(elementObjData);
			if ( rc != 0 )
			{
				System.out.println("Couold not find the elemenet\r\n");
				LogMsg="\t\tCouold not find the elemenet";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			String SourceXpath=elementObjData.getSourceValue();
			
			LogMsg="\t\tThe comboList SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			
			String Xpath=String.format(SourceXpath, 1);
			LogMsg="\t\tThe comboList Xpath = "+Xpath;
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			while(objectExistsWithXpath(TestDriver,Xpath))
			{
				WebElement textElement = findElemByXPath(TestDriver,Xpath);
				if( textElement.getText().equals(GoalText))
				{
					findFlag=true;
					LogMsg="\t\tThe element with the text exits successfully!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					break;
				}
				num++;
				Xpath=String.format(SourceXpath, num);
			}
			if(findFlag==false)
			{
				LogMsg="\t\tThe checkListTextExits "+GoalText+" not found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The Xpath list "+SourceXpath+" text "+GoalText +" cannot found!");
			}
		}
		return findFlag;
	}
	
	
	/**
	 * Check the test exist in Xpath list with only change one tag number
	 * 
	 * @param PageName
	 * @param ObjectName
	 * @param GoalText
	 * @return
	 */
	public static boolean checkListTextClick(String PageName,String ObjectName,String GoalText,boolean TestEnable)
	{
		boolean findFlag=false;
		if(TestStatusFlag)
		{
			String LogMsg="\tcomboListTestClick is called with PageName: "+PageName+", PbjectName:"+ObjectName+", GoalText:"+GoalText
					+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			int num=1;
			
			ElementObjectMapData elementObjData = new ElementObjectMapData(PageName,ObjectName,AppSWVersion);
			int rc = searchXpathFromObjectMap(elementObjData);
			if ( rc != 0 )
			{
				System.out.println("Couold not find the elemenet\r\n");
				LogMsg="\t\tCouold not find the elemenet";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			String SourceXpath=elementObjData.getSourceValue();
			LogMsg="\t\tThe comboList SourceValue = "+elementObjData.getSourceValue();
			LogInBuffer.add(LogMsg);
			
			String Xpath=String.format(SourceXpath, 1);
			LogMsg="\t\tThe comboList Xpath = "+Xpath;
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			while(objectExistsWithXpath(TestDriver,Xpath))
			{
				WebElement textElement = findElemByXPath(TestDriver,Xpath);
				if( textElement.getText().equals(GoalText))
				{
					findFlag=true;
					textElement.click();
					LogMsg="\t\tClick the element with the text successfully!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					break;
				}
				num++;
				Xpath=String.format(SourceXpath, num);
			}
			if(findFlag==false)
			{
				LogMsg="\t\tThe checkListTextClick "+GoalText+" not found!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				if(TestEnable)
					assertFail("The Xpath list "+SourceXpath+" text "+GoalText +" cannot found!");
			}
		}
		return findFlag;
	}
	
	
	/**
	 * Check alert whether exits
	 * @param TestEnable
	 */
	public static boolean alertExits(boolean TestEnable)
	{
		boolean result=false;
		if(TestStatusFlag)
		{
			String LogMsg="\talertExits is called at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			String CurrentView=getViewName(TestDriver);
			if(CurrentView.indexOf("NATIVE")!=-1)
			{
				result=true;
			}
			else
			{
				if(TestEnable)
				{
					LogMsg="\t\tThe Alert not found!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					if(TestEnable)
						assertFail("The alert cannot found!");
				}
			}
			
		}
		return result;
	}
	
	/**
	 * Check alert only accord the choice string
	 * @param ChoiceStr
	 * @param TestEnable
	 */
	public static void alertCheck(String ChoiceStr,boolean TestEnable)
	{
		if(TestStatusFlag)
		{
			String LogMsg="\talertCheck is called with ChoiceStr: "+ChoiceStr+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			String CurrentView=getViewName(TestDriver);
			if(CurrentView.equalsIgnoreCase("NATIVE_APP"))
			{
				LogMsg="\t\tSwitch to NATIVE_APP";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				TestDriver.switchTo().window("NATIVE_APP");
				VIEW_NAME="NATIVE_APP";
				LogMsg="\t\tSwitch to NATIVE_APP";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			// times out after 5 seconds
			WebDriverWait wait = new WebDriverWait(TestDriver, 5);
			// while the following loop runs, the DOM changes - 
			// page is refreshed, or element is removed and re-added
			try
			{
				wait.until(presenceOfElementLocated(By.id("message")));        
				// now we're good - let's click the element
				WebElement Alert=TestDriver.findElement(By.id("message"));
				if(Alert!=null)
				{
					System.out.println("The alert is:"+Alert.getText());
					for(int i=1;i<4;i++)
					{
						String ButtonIdStr=String.format("button%d", i);
						WebElement Button = TestDriver.findElement(By.id(ButtonIdStr));
						if(Button!=null)
						{
							System.out.println("The choice1 is:"+Button.getText());
							if(Button.getText().equalsIgnoreCase(ChoiceStr))
							{
								Button.click();
								LogMsg="\t\tClick the goal Button: "+ChoiceStr+" successfully!";
								LogInBuffer.add(LogMsg);
								updateLogText(LogMsg+"\n");
								break;
							}
						}
					}
					
					CurrentView=getViewName(TestDriver);
					if(CurrentView.equalsIgnoreCase("WEBVIEW_0"))
						TestDriver.switchTo().window("WEBVIEW_0");
				}
				else
				{
					LogMsg="\t\tThe Alert cannot be found!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					TestDriver.switchTo().window("WEBVIEW_0");
				}
			}
			catch (TimeoutException e)
			{
				LogMsg="\t\tWait for the alert time out!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				System.out.println("SEARCH FOR ELEMENT TEXT:timeout exception occurred");
				TestDriver.switchTo().window("WEBVIEW_0");
			}	
		}
	}
	
	/**
	 * Check the alert title exits and click the specific button to close the alert
	 * @param Title
	 * @param ChoiceStr
	 * @param TestEnable
	 */
	public static boolean alertCheck(String Title,String ChoiceStr,boolean TestEnable)
	{
		boolean TestResult = false;
		if(TestStatusFlag)
		{
			String LogMsg="\talertCheck is called with Title:"+Title+" ChoiceStr: "+ChoiceStr+" at time stamp: "+getCurrentTimeWithDate();
			LogInBuffer.add(LogMsg);
			updateLogText(LogMsg+"\n");
			
			String CurrentView=getViewName(TestDriver);
			if(CurrentView.equalsIgnoreCase("NATIVE_APP") && VIEW_NAME.indexOf("WEB")!=-1)
			{
				TestDriver.switchTo().window("NATIVE_APP");
				VIEW_NAME="NATIVE_APP";
				LogMsg="\t\tSwitch to NATIVE_APP";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
			}
			
			// times out after 5 seconds
			WebDriverWait wait = new WebDriverWait(TestDriver, 5);
			// while the following loop runs, the DOM changes - 
			// page is refreshed, or element is removed and re-added
			try
			{
				wait.until(presenceOfElementLocated(By.id("message")));        
				// now we're good - let's click the element
				WebElement Alert=TestDriver.findElement(By.id("message"));
				if(Alert!=null)
				{
					System.out.println(Alert.getText());
					String AlertTitle=Alert.getText();
					if(AlertTitle.indexOf(Title)!=-1)
					{
						for(int i=1;i<4;i++)
						{
							String ButtonIdStr=String.format("button%d", i);
							WebElement Button = TestDriver.findElement(By.id(ButtonIdStr));
							if(Button!=null)
							{
								System.out.println("The choice1 is:"+Button.getText());
								if(Button.getText().indexOf(ChoiceStr)!=-1)
								{
									Button.click();
									LogMsg="\t\tClick the goal Button: "+ChoiceStr+" successfully!";
									LogInBuffer.add(LogMsg);
									updateLogText(LogMsg+"\n");
									TestResult = true;
									break;
								}
							}
						}
					}
					else
					{
						LogMsg="\t\tThe alert message display not correct, real display:"+AlertTitle+", but the expected is "+Title;
						LogInBuffer.add(LogMsg);
						updateLogText(LogMsg+"\n");
						TestResult = false;
					}
					
					CurrentView=getViewName(TestDriver);
					if(CurrentView.equalsIgnoreCase("WEBVIEW_0"))
						TestDriver.switchTo().window("WEBVIEW_0");
				}
				else
				{
					LogMsg="\t\tThe Alert cannot be found!";
					LogInBuffer.add(LogMsg);
					updateLogText(LogMsg+"\n");
					TestDriver.switchTo().window("WEBVIEW_0");
				}
			}
			catch (TimeoutException e)
			{
				LogMsg="\t\tWait for the alert time out!";
				LogInBuffer.add(LogMsg);
				updateLogText(LogMsg+"\n");
				System.out.println("SEARCH FOR ELEMENT TEXT:timeout exception occurred");
				TestDriver.switchTo().window("WEBVIEW_0");
				
				if(TestEnable)
				{
					if(TestResult == false)
					{
						assertFail("The alert message display error!");
					}
				}
			}
		}
		return TestResult;
	}
	
	/**
	 *This API clears the app data so that the test can be started cleanly.
	 *  only needed to clear once in the main function before starting the
	 *  test sequence. It runs the PM command to clear the data.
	 * This should be called only once after the start gui is called. The startGUI
	 * gets the package name and that will be used to clear the data.
	 */
	public static String clearAppData()
	{
		String cmd = "adb shell pm clear " + AppPackage;
		String[] commandResult=new String[64];
		System.out.println("Running Command:"+cmd);
		int result;
		result = runWinCommnad(cmd,commandResult);
		if(result>0)
			return commandResult[0];
		else
			return null;
		
	}
	
	/**
	 * Start Appium for automation testing
	 * @param ShellFile
	 * @return
	 * @throws IOException 
	 */
	@SuppressWarnings("unused")
	public static String StartAppium(String ShellFile)
	{
		Runtime rt = Runtime.getRuntime();
		
		final String cmd = "cmd /c " + ShellFile;
		AppiumThread = new Thread(){
			public void run()
			{
				
				String fileName = System.getProperty("user.dir")+"\\TestResults\\AppiumLog.txt";
				File LogFile=new File(fileName);
				FileWriter fw;
				String command = "tasklist | grep node";
				String[] commandResult=new String[64];
				int result;
				result = runWinCommnad(command,commandResult);
				System.out.println("Result="+result);
				try {
					fw = new FileWriter(LogFile);
					AppiumLogPrinter = new PrintWriter(new FileWriter(fileName, true));
					Process proc = null;
					if(result>0)
					{
						try {
							proc = Runtime.getRuntime().exec("taskkill /F /T /IM node.exe");
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
					try {
						System.out.println("Running command:"+cmd);
						proc = Runtime.getRuntime().exec(cmd);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
					BufferedReader stdInput = new BufferedReader(new 
						     InputStreamReader(proc.getInputStream()));
					// read the output from the command
					String s = null;
					try {
						while ((s = stdInput.readLine()) != null && !ThreadStop) {
							 updateAppiumLogText(s+"\n");
							 AppiumLogPrinter.println(s);
						}
					}
					catch (IOException e1) 
					{
						e1.printStackTrace();
					}
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		       
			}
				
		};
		AppiumThread.start();
		return cmd;	
	}

	
	/**
	 * Set the start activity
	 * @param startActivity
	 */
	public static void SetStartActivity(String startActivity)
	{
		StartActivity = startActivity;
	}
}

