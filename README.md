# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: This project is creating a new automation test framwork. User just need know 22 basic common APIs, and call these API to do automation test
	API list:
		Alert:	
			alertCheck
		Button:
			buttonClick
			buttonExits
			buttonLongTimeClick
			buttonCompareText
			buttonGetText
		TextBox:
			textBoxExits
			textBoxCompareText
			textBoxGetText
		CheckBox:
			checkBoxExits
			checkBoxUncheck
			checkBoxCheck
		List:
			checkListTextExits
			clickListByText
		Combo:
			comboListExits
			comboListChoose
		InputBox:
			inputBoxExits
			inputBoxWrite
			inputBoxGetText
			inputBoxCompareText

		startTestCase
		stopTestCase

* Version: r0.0.1

### How do I get set up? ###

There is a Word file in the package, following the instruction to set up.
